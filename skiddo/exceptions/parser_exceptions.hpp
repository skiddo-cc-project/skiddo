#pragma once

#include <stdexcept>
#include <string>

class SyntaxError : public std::exception {
 private:
  std::string msg;

 public:
  explicit SyntaxError(const std::string& msg) : msg("SyntaxError: " + msg) {}

  const char* what() const throw() { return msg.c_str(); }
};

class ErrorToken : public SyntaxError {
  using SyntaxError::SyntaxError;
};

class EndMarkerToken : public std::exception {
  using std::exception::exception;

  const char* what() const throw() { return "Got ENDMARKER token"; }
};
