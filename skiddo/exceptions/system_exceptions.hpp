#pragma once

#include <skiddo/types/types.hpp>
#include <stdexcept>
#include <string>

class SystemError : public std::exception {};

class WhileBreakException : public SystemError {
  const char* what() const throw() { return "WhileBreak Exception"; }
};

class ReturnException : public SystemError {
 private:
  std::string msg;
  types::SkiddoObject payload;

 public:
  explicit ReturnException(types::SkiddoObject payload) : msg("Returned a value"), payload(payload) {}

  const char* what() const throw() { return msg.c_str(); }

  types::SkiddoObject getPayload() const { return payload; }
};
