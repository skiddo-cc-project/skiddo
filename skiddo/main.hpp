#pragma once

#include <string>

enum ProgramMode { INTERACTIVE_MODE, FILE_MODE };

struct ProgramConfig {
  ProgramMode mode;
  std::string filename;
};

ProgramConfig parse_args(int argc, char **argv);

int main(int argc, char **argv);
