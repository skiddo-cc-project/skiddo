#pragma once

#include "buffer.hpp"

namespace buffer {

class BufferCharReader {
 public:
  Buffer* buf;
  /* buf->buf <= cur <= buf->inp */
  size_t cur; /* Next character in the buffer */

  explicit BufferCharReader(Buffer* buf) : buf(buf), cur(0) {}

  /**
   * @brief Get next char, updating state; error code goes into tok->done
   *
   * @return int
   */
  int nextc();

  /**
   * @brief Back-up one character
   *
   * @param c character to be backup
   */
  void backup(int c);
};

}  // namespace buffer
