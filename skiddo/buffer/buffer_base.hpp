#pragma once

#include <cstdlib>
#include <string>

namespace buffer {

class Buffer {
 public:
  /* buf <= inp <= end */
  char* buf; /* Input buffer or NULL */
  char* inp; /* End of data in buffer */

 private:
  const char* end; /* End of input buffer if buf != NULL */

 protected:
  /**
   * @brief Reserve free buffer space
   *
   * @param size size of needed space
   */
  void reserve(size_t size);

 public:
  Buffer() : buf(NULL), inp(NULL), end(NULL) {}
  Buffer(const char* data, size_t size) : Buffer() { this->push(data, size); }
  explicit Buffer(const std::string& buf) : Buffer() { this->push(buf); }
  virtual ~Buffer();

  /**
   * @brief Call this to fill the buffer with new data
   *
   * Not implemented in basic buffer
   */
  virtual bool read_more() { return false; }

  /**
   * @brief Push data to the buffer
   *
   * @param data data to be pushed to buffer
   * @param size size of data
   */
  void push(const char* data, size_t size);

  /**
   * @brief Push data to the buffer
   *
   * @param data data to be pushed to buffer
   */
  void push(const std::string& data) { push(data.c_str(), data.size()); }

  char* get(size_t ptr) { return this->buf + ptr; }

  std::string get(size_t start, size_t end) { return std::string(this->buf + start, this->buf + end); }

  size_t size() { return this->inp - this->buf; }
};

}  // namespace buffer
