#pragma once

#include <memory>
#include <skiddo/types/types.hpp>
#include <string>
#include <unordered_map>
#include <vector>

namespace execution {

class ProgramContextCore {
 private:
  ProgramContextCore* parent;
  std::unordered_map<std::string, types::SkiddoObject> ctx;

  explicit ProgramContextCore(ProgramContextCore* parent) : parent(parent) {}

  /**
   * @brief re-register the new object in the context
   */
  bool reregister_func(const std::string& name, const types::SkiddoObject& func);

 public:
  ProgramContextCore() : parent(NULL) {}

  /**
   * @brief Register new object in the current context
   */
  void register_func(const std::string& name, const types::SkiddoObject& func, bool local_only = false);

  /**
   * @brief Get the registed object by name
   */
  types::SkiddoObject get_func(const std::string& name);

  /**
   * @brief Create new context based on current one (aka local context)
   */
  ProgramContextCore* new_context() { return new ProgramContextCore(this); }
};

struct ProgramState {
  bool has_keyboard_interrupt = false;
  std::vector<types::SkiddoObject> traceback;
};

class ProgramContext {
 private:
  std::shared_ptr<ProgramContextCore> ctx;
  std::shared_ptr<ProgramState> state;
  explicit ProgramContext(ProgramContext* parent) : ctx(parent->ctx->new_context()), state(parent->state) {}

 public:
  ProgramContext() : ctx(new ProgramContextCore()), state(new ProgramState()) {}

  /**
   * @brief Create context with standard set of predefined functions
   */
  static ProgramContext standard_state();

  /**
   * @brief Register new object in the current context
   */
  void register_func(const std::string& name, const types::SkiddoObject& func, bool local_only = false) {
    ctx->register_func(name, func, local_only);
  }

  /**
   * @brief Register new object in the current context
   */
  void register_func(const types::SkiddoObject& name, const types::SkiddoObject& func, bool local_only = false) {
    ctx->register_func(name.as_atom()->name, func, local_only);
  }

  /**
   * @brief Get the registed object by name
   */
  types::SkiddoObject get_func(const std::string& name) { return ctx->get_func(name); }

  /**
   * @brief Get the registed object by name
   */
  types::SkiddoObject get_func(const types::SkiddoObject& name) { return ctx->get_func(name.as_atom()->name); }

  /**
   * @brief Create new context based on current one (aka local context)
   */
  ProgramContext new_local_state() { return ProgramContext(this); }

  ProgramState* get_state() { return state.get(); }
};

}  // namespace execution
