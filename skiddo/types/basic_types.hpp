#pragma once

#include <memory>
#include <skiddo/exceptions/runtime_exceptions.hpp>
#include <sstream>
#include <string>
#include <vector>

#include "core_types.hpp"
#include "enums.hpp"

namespace types {

class Atom : public Function {
 protected:
  virtual SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<SkiddoObject>& args);
  virtual void print(std::ostream& out) const { out << name; }

 public:
  std::string name;

  explicit Atom(const std::string& name) : Function("<atom>"), name(name) {}
  virtual Type get_type() const { return Type::ATOM; }
};

class LiteralNull;
class LiteralBool;
class LiteralInt;
class LiteralFloat;

class Literal : public Function {
 protected:
  virtual SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<SkiddoObject>& args);

 public:
  Literal() : Function("<literal>") {}
  virtual Type get_type() const { return Type::LITERAL; }
  virtual LiteralType get_literal_type() const = 0;

  LiteralNull* as_null();
  LiteralBool* as_bool();
  LiteralInt* as_int();
  LiteralFloat* as_float();

  template <typename T>
  T convertValueTo() const;
};

class List : public Function {
 protected:
  virtual SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<SkiddoObject>& args);

  virtual void print(std::ostream& out) const {
    out << "(";
    for (uint i = 0; i < items.size(); i++) {
      if (i != 0) out << " ";
      out << items[i];
    }
    out << ")";
  }

 public:
  std::vector<SkiddoObject> items;

  explicit List(const std::vector<SkiddoObject>& items) : Function("<list>"), items(items) {}
  virtual Type get_type() const { return Type::LIST; }
};

class Quote : public Function {
 protected:
  virtual SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<SkiddoObject>& args);
  virtual void print(std::ostream& out) const { out << '\'' << item; }

 public:
  SkiddoObject item;

  explicit Quote(const SkiddoObject& item) : Function("<quote>"), item(item) {}
  virtual Type get_type() const { return Type::QUOTE; }
};

class LiteralNull : public Literal {
 private:
  bool show_in_output;

 protected:
  virtual void print(std::ostream& out) const { out << "null"; }

 public:
  explicit LiteralNull(const bool& show_in_output) : show_in_output(show_in_output) {}
  virtual LiteralType get_literal_type() const { return LiteralType::LITERAL_NULL; }
  virtual bool need_to_show_in_output() { return show_in_output; }
};

class LiteralBool : public Literal {
 protected:
  virtual void print(std::ostream& out) const { out << (value ? "true" : "false"); }

 public:
  bool value;

  explicit LiteralBool(const bool& value) : value(value) {}
  virtual LiteralType get_literal_type() const { return LiteralType::LITERAL_BOOL; }
};

class LiteralInt : public Literal {
 protected:
  virtual void print(std::ostream& out) const { out << value; }

 public:
  int64_t value;

  explicit LiteralInt(const int64_t& value) : value(value) {}
  virtual LiteralType get_literal_type() const { return LiteralType::LITERAL_INT; }
};

class LiteralFloat : public Literal {
 protected:
  virtual void print(std::ostream& out) const { out << value; }

 public:
  double value;

  explicit LiteralFloat(const double& value) : value(value) {}
  virtual LiteralType get_literal_type() const { return LiteralType::LITERAL_FLOAT; }
};

template <typename T>
T Literal::convertValueTo() const {
  switch (get_literal_type()) {
    case LITERAL_NULL:
      throw TypeError("Attempt to convert null");
    case LITERAL_BOOL:
      return static_cast<T>(reinterpret_cast<const LiteralBool*>(this)->value);
    case LITERAL_INT:
      return static_cast<T>(reinterpret_cast<const LiteralInt*>(this)->value);
    case LITERAL_FLOAT:
      return static_cast<T>(reinterpret_cast<const LiteralFloat*>(this)->value);
    default:
      throw TypeError("Unknown literal type");
  }
}

}  // namespace types
