#pragma once

namespace types {

enum Type {
  FUNCTION = 0,
  ATOM = 1,
  LITERAL = 2,
  LIST = 3,
  QUOTE = 4,
};

enum LiteralType {
  LITERAL_NULL = 0,
  LITERAL_BOOL = 1,
  LITERAL_INT = 2,
  LITERAL_FLOAT = 3,
};

const char* literalToString(LiteralType type);

}  // namespace types
