#pragma once

#include <string>
#include <utility>
#include <vector>

#include "basic_types.hpp"

namespace types {

class FunctionWithContext : public Function {
 protected:
  virtual SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<SkiddoObject>& args);

 public:
  std::vector<std::string> local_vars;
  const std::vector<SkiddoObject> body;

  FunctionWithContext(const std::string& function_name, const std::vector<std::string>& local_vars,
                      const std::vector<SkiddoObject>& body)
      : Function(function_name), local_vars(local_vars), body(body) {}

  static SkiddoObject create(const std::string& function_name, const std::vector<std::string>& local_vars,
                             const std::vector<SkiddoObject>& body) {
    return SkiddoObject(new FunctionWithContext(function_name, local_vars, body));
  }
};

}  // namespace types
