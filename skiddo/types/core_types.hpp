#pragma once

#include <memory>
#include <skiddo/exceptions/runtime_exceptions.hpp>
#include <sstream>
#include <string>
#include <vector>

#include "enums.hpp"

namespace execution {
class ProgramContext;
}

namespace types {

class SkiddoObject;

/**
 * @brief Abstract function
 *
 * In Skiddo language all stuff are functions. This class is the base class for
 * any function in Skiddo language.
 */
class Function {
  friend class SkiddoObject;

 private:
  void set_self(const std::weak_ptr<Function>& self) { this->self = self; }

 protected:
  std::weak_ptr<Function> self;
  std::string function_name;

  virtual SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<SkiddoObject>& args) = 0;
  virtual void print(std::ostream& out) const { out << "<function " << function_name << " at " << this << ">"; }

 public:
  explicit Function(const std::string& function_name) : function_name(function_name) {}
  virtual ~Function() {}

  SkiddoObject execute(execution::ProgramContext ctx, const std::vector<SkiddoObject>& args);

  SkiddoObject to_object();

  uintptr_t get_id() const { return reinterpret_cast<std::uintptr_t>(this); }

  virtual Type get_type() const { return Type::FUNCTION; }

  std::string get_function_name() { return function_name; }

  /**
   * @brief Whether to display this object in the output
   */
  virtual bool need_to_show_in_output() { return true; }

  std::string to_str() const {
    std::stringstream ss;
    print(ss);
    return ss.str();
  }

  friend std::ostream& operator<<(std::ostream& out, const Function& f) {
    f.print(out);
    return out;
  }
};

class Atom;
class Literal;
class List;
class Quote;

typedef std::shared_ptr<Function> PFunction;

/**
 * @brief Universal wrapper for any function in Skiddo language
 *
 * The wrapper can be: abstract function, atom, list, quote, literal, function
 * with context
 *
 * Class use std::shared_ptr for memory management
 */
class SkiddoObject : private PFunction {
 public:
  SkiddoObject() : PFunction() {}
  explicit SkiddoObject(Function* obj) : PFunction(obj) { obj->set_self(*this); }
  explicit SkiddoObject(const PFunction& self) : PFunction(self) {}

  static SkiddoObject nothing();  // null, but do not shown in the output
  static SkiddoObject atom(const std::string& name);
  static SkiddoObject list(const std::vector<SkiddoObject>& items);
  static SkiddoObject quote(const SkiddoObject& item);
  static SkiddoObject literal();
  static SkiddoObject literal(const bool& value);
  static SkiddoObject literal(const int32_t& value) { return literal(static_cast<int64_t>(value)); }
  static SkiddoObject literal(const int64_t& value);
  static SkiddoObject literal(const float& value) { return literal(static_cast<double>(value)); }
  static SkiddoObject literal(const double& value);

  uintptr_t get_id() const { return (*this)->get_id(); }
  Type get_type() const { return (*this)->get_type(); }
  std::string get_function_name() { return (*this)->get_function_name(); }

  /**
   * @brief Whether to display this object in the output
   */
  bool need_to_show_in_output() { return (*this)->need_to_show_in_output(); }

  std::string to_str() const { return (*this)->to_str(); }

  Function* get() const { return PFunction::get(); }
  Atom* as_atom() const;
  Literal* as_literal() const;
  List* as_list() const;
  Quote* as_quote() const;

  SkiddoObject execute(execution::ProgramContext ctx, const std::vector<SkiddoObject>& args) const;

  friend std::ostream& operator<<(std::ostream& out, const SkiddoObject& f) { return out << *f; }
};

}  // namespace types
