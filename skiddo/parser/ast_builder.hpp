#pragma once

#include <skiddo/buffer/buffer.hpp>
#include <skiddo/parser/tokenizer.hpp>
#include <skiddo/types/types.hpp>

namespace parser {

class ASTBuilder {
 private:
  buffer::Buffer* buffer;
  Tokenizer* tokenizer;

  types::SkiddoObject parse_any(parser::Token t, size_t start, size_t end);
  types::SkiddoObject parse_atom(parser::Token t, size_t start, size_t end);
  types::SkiddoObject parse_number(parser::Token t, size_t start, size_t end);
  types::SkiddoObject parse_bool(parser::Token t, size_t start, size_t end);
  types::SkiddoObject parse_null(parser::Token t, size_t start, size_t end);
  types::SkiddoObject parse_list(parser::Token t, size_t start, size_t end);
  types::SkiddoObject parse_quote(parser::Token t, size_t start, size_t end);

 public:
  explicit ASTBuilder(buffer::Buffer* buffer, Tokenizer* tokenizer) : buffer(buffer), tokenizer(tokenizer) {}

  types::SkiddoObject get();
};

}  // namespace parser
