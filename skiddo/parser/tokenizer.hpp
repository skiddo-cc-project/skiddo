#pragma once

#include "../buffer/char_reader.hpp"
#include "token.hpp"

namespace parser {

class Tokenizer {
 private:
  buffer::BufferCharReader buf;

  int lineno; /* Current line number */

 public:
  explicit Tokenizer(buffer::Buffer* buf);

  /**
   * @brief Get next token in buffer
   *
   * Returns a token, or ENDMARKER if there are no more tokens in the buffer
   *
   * @param start pointer to start of token
   * @param end pointer to end of token
   * @return token type
   */
  Token get(size_t* start, size_t* end);
};

}  // namespace parser
