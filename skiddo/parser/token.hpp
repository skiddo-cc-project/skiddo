#pragma once

#include <cstdlib>
#include <string>

namespace parser {

enum Token {
  ENDMARKER = 0,  // No more tokens
  NAME = 1,       // Identifier (most frequent token!)
  NIL = 2,        // null
  BOOL = 3,       // true or false
  INTEGER = 4,    // 42
  FLOAT = 5,      // 23.43
  LPAR = 6,       // (
  RPAR = 7,       // )
  QUOTE = 8,      // Unevaluated literal
};

extern const char* const TokenNames[]; /* Token names */

const char* const string_true = "true";
const char* const string_false = "false";
const char* const string_null = "null";

}  // namespace parser
