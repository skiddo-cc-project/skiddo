# Reference: https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html
# Reference: https://gitlab.iap.kit.edu/AirShowerPhysics/corsika/-/wikis/Improve%20Coverage
# Reference: https://github.com/google/googletest/tree/master/googletest#incorporating-into-an-existing-cmake-project
# Reference: https://gitlab.kitware.com/cmake/community/-/wikis/doc/ctest/Testing-With-CTest

option(BUILD_TESTS "Build the unit tests" OFF)

if (CMAKE_BUILD_TYPE STREQUAL Coverage)
  # search for local gcovr
  find_program (gcovr_bin gcovr)
  if (NOT gcovr_bin)
    message(FATAL_ERROR "gcovr not found. Please, install the program and repeat")
  endif ()

  add_custom_command(
    OUTPUT coverage-xml-cmd
    COMMAND ${gcovr_bin} --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml -e ${CMAKE_BINARY_DIR} -e ${CMAKE_SOURCE_DIR}/test --root ${CMAKE_SOURCE_DIR}
    )
  add_custom_target(coverage-xml DEPENDS coverage-xml-cmd)

  add_custom_command(
    OUTPUT coverage-report-cmd
    COMMAND ${gcovr_bin} --exclude-unreachable-branches -e ${CMAKE_BINARY_DIR} -e ${CMAKE_SOURCE_DIR}/test --root ${CMAKE_SOURCE_DIR}
    )
  add_custom_target(coverage DEPENDS coverage-report-cmd)
endif()


if(BUILD_TESTS)
  include(CTest)
  
  include(FetchContent)
  FetchContent_Declare(
    googletest
    URL https://github.com/google/googletest/archive/refs/tags/release-1.11.0.zip
  )

  # For Windows: Prevent overriding the parent project's compiler/linker settings
  set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
  FetchContent_MakeAvailable(googletest)

  file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/test_outputs/)
  set(CTEST_OUTPUT_ON_FAILURE 1)

  function(skiddo_add_test name)
    cmake_parse_arguments(PARSE_ARGV 1 "ARG" "" "SANITIZE" "SOURCES;DEPENDS")

    if(NOT ARG_SOURCES)
      message(FATAL_ERROR "skiddo_add_test with no sources")
    endif()

    if(NOT ARG_SANITIZE)
      set(sanitize "address,undefined")
    else()
      set(sanitize ${ARG_SANITIZE})
    endif()

    add_executable(${name} ${ARG_SOURCES})

    if(ARG_DEPENDS)
      target_link_libraries(${name} PUBLIC gtest_main ${ARG_DEPENDS})
    endif()

    target_compile_options(${name} PRIVATE -g) # do not skip asserts
    # -O1 is suggested in clang docs to get reasonable performance
    target_compile_options(${name} PRIVATE -O1 -fno-omit-frame-pointer -fsanitize=${sanitize} -fno-sanitize-recover=all)
    set_target_properties(${name} PROPERTIES LINK_FLAGS "-fsanitize=${sanitize}")
    add_test(NAME ${name} COMMAND ${name} --gtest_output=xml:${PROJECT_BINARY_DIR}/test_outputs/${name}.xml)
  endfunction(skiddo_add_test)

  add_subdirectory(test)
endif()