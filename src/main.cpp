#include <csignal>
#include <cstring>
#include <iostream>
#include <memory>
#include <skiddo/buffer/buffer.hpp>
#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/main.hpp>
#include <skiddo/parser/ast_builder.hpp>
#include <skiddo/parser/tokenizer.hpp>
#include <skiddo/types/types.hpp>

using execution::ProgramContext;
using types::SkiddoObject;

ProgramContext ctx;

void keyboard_interrupt_handler([[maybe_unused]] int s) {
  if (ctx.get_state()->has_keyboard_interrupt) exit(1);
  ctx.get_state()->has_keyboard_interrupt = true;
}

ProgramConfig parse_args(int argc, char** argv) {
  switch (argc) {
    case 1:
      return {INTERACTIVE_MODE, ""};
    case 2:
      if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
        printf(
            "usage: <> [-h] file\n\nSimple skiddo programming language "
            "compiler\n\nPositional arguments\n\tfile\n\nOptional "
            "arguments\n\t-h, --help show this help message and exit\n");
        exit(EXIT_SUCCESS);
      } else {
        return {FILE_MODE, std::string(argv[1])};
      }
      break;
    default:
      throw FileError("Too many arguments");
  }
}

void print_traceback() {
  execution::ProgramState& state = *ctx.get_state();

  fprintf(stderr, "Traceback: ");
  for (uint i = 0; i < state.traceback.size(); i++) {
    if (i != 0) fprintf(stderr, " -> ");
    std::string function_name = state.traceback[i].get_function_name();
    fprintf(stderr, "%s", function_name.c_str());
  }
  fprintf(stderr, "\n");
}

int main([[maybe_unused]] int args, [[maybe_unused]] char** argv) {
  ProgramConfig cfg = parse_args(args, argv);

  // Setup hook for Ctrl + C
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = keyboard_interrupt_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);

  std::shared_ptr<buffer::Buffer> buffer;
  if (cfg.mode == INTERACTIVE_MODE) {
    buffer.reset(new buffer::InteractiveBuffer());
  } else {
    buffer.reset(new buffer::FileBuffer(cfg.filename));
  }

  parser::Tokenizer tokenizer(buffer.get());
  parser::ASTBuilder ast_builder(buffer.get(), &tokenizer);
  ctx = ProgramContext::standard_state();

  while (true) {
    execution::ProgramState& state = *ctx.get_state();

    try {
      SkiddoObject tree = ast_builder.get();
      SkiddoObject res = tree.execute(ctx, {});

      if (res.need_to_show_in_output()) {
        std::string output = res.to_str();
        printf("%s\n", output.c_str());
      }

      // check state after execution
      if (state.traceback.size() != 0) throw FatalError("traceback is not empty");
    } catch (EndMarkerToken& e) {
      break;
    } catch (KeyboardInterrupt& e) {
      print_traceback();
      fprintf(stderr, "%s\n", e.what());
      ctx.get_state()->has_keyboard_interrupt = false;
    } catch (FatalError& e) {
      fprintf(stderr, "%s\n", e.what());
      return 2;
    } catch (std::exception& e) {
      print_traceback();
      fprintf(stderr, "%s\n", e.what());
      if (cfg.mode != INTERACTIVE_MODE) return 1;
      state.traceback.clear();  // clear traceback
    }
  }

  return 0;
}
