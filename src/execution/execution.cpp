#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>

#include "standard_functions/standard_functions.hpp"

using execution::ProgramContextCore, execution::ProgramContext;
using types::Type;

bool ProgramContextCore::reregister_func(const std::string& name, const types::SkiddoObject& func) {
  if (ctx.count(name)) {
    ctx[name] = func;
    return true;
  }
  if (parent) {
    return parent->reregister_func(name, func);
  }
  return false;
}

void ProgramContextCore::register_func(const std::string& name, const types::SkiddoObject& func, bool local_only) {
  if (local_only || !reregister_func(name, func)) {
    ctx[name] = func;
  }
}

types::SkiddoObject ProgramContextCore::get_func(const std::string& name) {
  const auto& it = ctx.find(name);
  if (it != ctx.end()) return it->second;
  if (parent != NULL) return parent->get_func(name);
  throw NameError("name " + name + " is not defined");
}

ProgramContext ProgramContext::standard_state() {
  ProgramContext res;
  execution::init_standard_context(res);
  return res;
}
