#include "predicate_functions.hpp"

using execution::ProgramContext;
using types::Type, types::Function, types::SkiddoObject, types::LiteralType;

SkiddoObject PredicateFunction::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 1)
    throw TypeError(function_name + " takes 1 argument but " + std::to_string(args.size()) + " were given");

  types::SkiddoObject obj = args[0].execute(ctx, {});
  if (obj.get_type() != type) return types::SkiddoObject::literal(false);

  if (type == Type::LITERAL) return types::SkiddoObject::literal(obj.as_literal()->get_literal_type() == literal_type);

  return types::SkiddoObject::literal(true);
}
