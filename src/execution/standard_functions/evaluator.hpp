#pragma once

#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>
#include <vector>

/**
 * @brief The function evaluate its first argument and return the result.
 *
 * ( eval Element )
 *
 * After evaluation, the argument should be of any type. If the argument if a
 * list then the function treats it as a valid program and tries to evaluate
 * it. In that case, the function returns the value that the program issues. If
 * the argument is a literal or atom the function just return the argument.
 */
class EvalFunction : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};
