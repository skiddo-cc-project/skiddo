#pragma once

#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>
#include <string>
#include <vector>

/**
 * @brief Checks if evaluated argument has type T
 *
 * Returns a boolean value. Example:
 * (isatom atom)
 */

class PredicateFunction : public types::Function {
 private:
  types::Type type;
  types::LiteralType literal_type = types::LiteralType::LITERAL_NULL;

 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  PredicateFunction(const std::string& function_name, types::Type type) : types::Function(function_name), type(type) {}
  PredicateFunction(const std::string& function_name, types::LiteralType type)
      : types::Function(function_name), type(types::Type::LITERAL), literal_type(type) {}
};
