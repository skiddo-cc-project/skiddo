#include "arithmetic_functions.hpp"

#include <skiddo/exceptions/exceptions.hpp>
#include <string>
#include <vector>

using execution::ProgramContext;
using types::Type, types::Function, types::SkiddoObject, types::Literal, types::LiteralFloat, types::LiteralInt,
    types::LiteralBool, types::LiteralType;

template <typename T>
T ArithmeticOperation::perform_operation(T left, T right) {
  switch (type) {
    case PLUS:
      return left + right;
    case MINUS:
      return left - right;
    case DIVIDE:
      if (right == 0) throw ZeroDivisionError("Can not divide by zero");
      return left / right;
    case TIMES:
      return left * right;
  }
  throw FatalError("Unknown operator type");
}

SkiddoObject ArithmeticOperation::_execute(execution::ProgramContext ctx,
                                           const std::vector<types::SkiddoObject> &args) {
  if (args.size() != 2)
    throw TypeError(function_name + " takes 2 arguments but " + std::to_string(args.size()) + " was given");

  SkiddoObject number1 = args[0].execute(ctx, {});
  SkiddoObject number2 = args[1].execute(ctx, {});

  const types::LiteralType number1Type = number1.as_literal()->get_literal_type();
  const types::LiteralType number2Type = number2.as_literal()->get_literal_type();
  if (number1Type != LiteralType::LITERAL_INT && number1Type != LiteralType::LITERAL_FLOAT)
    throw TypeError(function_name + " expects first number to be int/float type");
  if (number2Type != LiteralType::LITERAL_INT && number2Type != LiteralType::LITERAL_FLOAT)
    throw TypeError(function_name + " expects second number to be int/float type");

  if (number1Type == LiteralType::LITERAL_FLOAT || number2Type == LiteralType::LITERAL_FLOAT || type == DIVIDE) {
    double number1Double = number1.as_literal()->convertValueTo<double>();
    double number2Double = number2.as_literal()->convertValueTo<double>();
    return SkiddoObject::literal(perform_operation(number1Double, number2Double));
  }
  int number1Int = number1.as_literal()->convertValueTo<int>();
  int number2Int = number2.as_literal()->convertValueTo<int>();
  return SkiddoObject::literal(perform_operation(number1Int, number2Int));
}
