#include "special_forms.hpp"

#include <memory>
#include <skiddo/exceptions/exceptions.hpp>
#include <string>

using execution::ProgramContext;
using types::Type, types::Function, types::SkiddoObject, types::Literal, types::LiteralFloat, types::LiteralInt,
    types::LiteralBool, types::LiteralType;

std::vector<std::string> prepare_local_vars(const SkiddoObject& local_vars) {
  const std::vector<SkiddoObject>& items = local_vars.as_list()->items;

  std::vector<std::string> res;
  res.reserve(items.size());
  for (const SkiddoObject& obj : items) res.push_back(obj.as_atom()->name);

  return res;
}

SkiddoObject QuoteFunction::_execute([[maybe_unused]] ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 1)
    throw TypeError(function_name + " takes 1 arguments but " + std::to_string(args.size()) + " was given");
  return args[0];
}

SkiddoObject PrintFunction::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() > 1)
    throw TypeError(function_name + " takes 0 or 1 argument but " + std::to_string(args.size()) + " was given");

  if (args.size() == 0) {
    printf("\n");
    return SkiddoObject::nothing();
  }

  const SkiddoObject argument = args[0].execute(ctx, {});
  std::string output = argument.to_str();
  printf("%s\n", output.c_str());

  return SkiddoObject::nothing();
}

SkiddoObject DefineFunction::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 2)
    throw TypeError(function_name + " takes 2 arguments but " + std::to_string(args.size()) + " was given");
  ctx.register_func(args[0], args[1].execute(ctx, {}));
  return SkiddoObject::nothing();
}

SkiddoObject CustomFunction::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 3)
    throw TypeError(function_name + " takes 3 arguments but " + std::to_string(args.size()) + " was given");

  const std::string& name = args[0].as_atom()->name;
  const SkiddoObject func = types::FunctionWithContext::create(name, prepare_local_vars(args[1]), {args[2]});
  ctx.register_func(args[0], func);
  return SkiddoObject::nothing();
}

SkiddoObject LambdaFunction::_execute([[maybe_unused]] ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 2)
    throw TypeError(function_name + " takes 2 arguments but " + std::to_string(args.size()) + " was given");

  return types::FunctionWithContext::create("<lambda>", prepare_local_vars(args[0]), {args[1]});
}

SkiddoObject ProgFunction::_execute([[maybe_unused]] ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 2)
    throw TypeError(function_name + " takes 2 arguments but " + std::to_string(args.size()) + " was given");

  return types::FunctionWithContext::create("<prog>", prepare_local_vars(args[0]), args[1].as_list()->items);
}

SkiddoObject CondFunction::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() > 3 || args.size() < 2)
    throw TypeError(function_name + " takes 2 or 3 arguments but " + std::to_string(args.size()) + " was given");

  SkiddoObject condition = args[0].execute(ctx, {});

  if (condition.as_literal()->as_bool()->value) return args[1].execute(ctx, {});
  if (args.size() == 3) return args[2].execute(ctx, {});
  return SkiddoObject::nothing();
}

SkiddoObject While::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 2)
    throw TypeError(function_name + " takes 2 arguments but " + std::to_string(args.size()) + " was given");

  SkiddoObject condition = args[0].execute(ctx, {});

  while (condition.as_literal()->as_bool()->value) {
    try {
      args[1].execute(ctx, {});
    } catch (WhileBreakException& e) {
      break;
    }
    condition = args[0].execute(ctx, {});
  }

  return SkiddoObject::nothing();
}

SkiddoObject ReturnFunction::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 1)
    throw TypeError(function_name + " takes 1 argument but " + std::to_string(args.size()) + " was given");

  const SkiddoObject argument = args[0].execute(ctx, {});
  throw ReturnException(argument);
}

SkiddoObject Break::_execute([[maybe_unused]] ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 0)
    throw TypeError(function_name + " takes 0 arguments but " + std::to_string(args.size()) + " was given");

  throw WhileBreakException();
}
