#include "comparison_function.hpp"

#include <cmath>
#include <string>

using execution::ProgramContext;
using types::Type, types::Function, types::SkiddoObject, types::LiteralType;

template <typename T>
bool ComparisonFunction::perform_comparison(T left, T right) {
  switch (type) {
    case EQUAL:
      return left == right;
    case NONEQ:
      return left != right;
    case LESS:
      return left < right;
    case LESSEQ:
      return left <= right;
    case GREATER:
      return left > right;
    case GREATEREQ:
      return left >= right;
  }
  return false;
}

SkiddoObject ComparisonFunction::_execute(ProgramContext ctx, const std::vector<types::SkiddoObject>& args) {
  if (args.size() != 2)
    throw TypeError(function_name + " takes 2 arguments but " + std::to_string(args.size()) + " was given");

  const SkiddoObject firstObj = args[0].execute(ctx, {});
  const SkiddoObject secondObj = args[1].execute(ctx, {});

  const types::LiteralType firstType = firstObj.as_literal()->get_literal_type();
  const types::LiteralType secondType = secondObj.as_literal()->get_literal_type();

  if (firstType == LiteralType::LITERAL_NULL || secondType == LiteralType::LITERAL_NULL) {
    if (type == EQUAL) {
      return SkiddoObject::literal(firstType == secondType);
    } else if (type == NONEQ) {
      return SkiddoObject::literal(firstType != secondType);
    } else {
      const std::string firstTypeStr = types::literalToString(firstType);
      const std::string secondTypeStr = types::literalToString(secondType);
      throw TypeError("There is no comparison operation between types " + firstTypeStr + " and " + secondTypeStr);
    }
  }

  LiteralType resultType = (firstType < secondType) ? secondType : firstType;

  switch (resultType) {
    case LiteralType::LITERAL_BOOL: {
      const bool left = firstObj.as_literal()->convertValueTo<bool>();
      const bool right = secondObj.as_literal()->convertValueTo<bool>();
      return SkiddoObject::literal(perform_comparison(left, right));
    }
    case LiteralType::LITERAL_INT: {
      const int left = firstObj.as_literal()->convertValueTo<int>();
      const int right = secondObj.as_literal()->convertValueTo<int>();
      return SkiddoObject::literal(perform_comparison(left, right));
    }
    case LiteralType::LITERAL_FLOAT: {
      const double left = firstObj.as_literal()->convertValueTo<double>();
      const double right = secondObj.as_literal()->convertValueTo<double>();
      return SkiddoObject::literal(perform_comparison(left, right));
    }
    default:
      throw TypeError("Unknown result type of given operation");
  }
}
