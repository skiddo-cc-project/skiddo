#include "standard_functions.hpp"

#include <utility>

#include "arithmetic_functions.hpp"
#include "comparison_function.hpp"
#include "evaluator.hpp"
#include "list_functions.hpp"
#include "logical_operators.hpp"
#include "predicate_functions.hpp"
#include "special_forms.hpp"

using execution::ProgramContext;
using types::SkiddoObject;
using types::Type, types::LiteralType;

template <typename T, typename... Args>
SkiddoObject create(Args&&... args) {
  return SkiddoObject(new T(std::forward<Args>(args)...));
}

void execution::init_standard_context(ProgramContext ctx) {
  // special_forms
  ctx.register_func("quote", create<QuoteFunction>("quote"));
  ctx.register_func("print", create<PrintFunction>("print"));
  ctx.register_func("setq", create<DefineFunction>("setq"));
  ctx.register_func("func", create<CustomFunction>("func"));
  ctx.register_func("lambda", create<LambdaFunction>("lambda"));
  ctx.register_func("prog", create<ProgFunction>("prog"));
  ctx.register_func("cond", create<CondFunction>("cond"));
  ctx.register_func("while", create<While>("while"));
  ctx.register_func("return", create<ReturnFunction>("return"));
  ctx.register_func("break", create<Break>("break"));

  // arithmetic operations
  ctx.register_func("plus", create<ArithmeticOperation>("plus", ArithmeticOperation::Op::PLUS));
  ctx.register_func("minus", create<ArithmeticOperation>("minus", ArithmeticOperation::Op::MINUS));
  ctx.register_func("times", create<ArithmeticOperation>("times", ArithmeticOperation::Op::TIMES));
  ctx.register_func("divide", create<ArithmeticOperation>("divide", ArithmeticOperation::Op::DIVIDE));

  // list functions
  ctx.register_func("head", create<HeadFunction>("head"));
  ctx.register_func("tail", create<TailFunction>("tail"));
  ctx.register_func("cons", create<ConsFunction>("cons"));

  // comparison
  ctx.register_func("equal", create<ComparisonFunction>("equal", ComparisonFunction::Op::EQUAL));
  ctx.register_func("nonequal", create<ComparisonFunction>("nonequal", ComparisonFunction::Op::NONEQ));
  ctx.register_func("less", create<ComparisonFunction>("less", ComparisonFunction::Op::LESS));
  ctx.register_func("lesseq", create<ComparisonFunction>("lesseq", ComparisonFunction::Op::LESSEQ));
  ctx.register_func("greater", create<ComparisonFunction>("greater", ComparisonFunction::Op::GREATER));
  ctx.register_func("greatereq", create<ComparisonFunction>("greatereq", ComparisonFunction::Op::GREATEREQ));

  // predicate functions
  ctx.register_func("isint", create<PredicateFunction>("isint", LiteralType::LITERAL_INT));
  ctx.register_func("isreal", create<PredicateFunction>("isreal", LiteralType::LITERAL_FLOAT));
  ctx.register_func("isbool", create<PredicateFunction>("isbool", LiteralType::LITERAL_BOOL));
  ctx.register_func("isnull", create<PredicateFunction>("isnull", LiteralType::LITERAL_NULL));
  ctx.register_func("isatom", create<PredicateFunction>("isatom", Type::ATOM));
  ctx.register_func("islist", create<PredicateFunction>("islist", Type::LIST));

  // logical operators
  ctx.register_func("and", create<BinaryLogicFunction>("and", BinaryLogicFunction::Op::AND));
  ctx.register_func("or", create<BinaryLogicFunction>("or", BinaryLogicFunction::Op::OR));
  ctx.register_func("xor", create<BinaryLogicFunction>("xor", BinaryLogicFunction::Op::XOR));
  ctx.register_func("not", create<UnaryLogicFunction>("xor", UnaryLogicFunction::Op::NOT));

  // evaluator
  ctx.register_func("eval", create<EvalFunction>("eval"));
}
