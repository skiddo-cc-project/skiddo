#include "list_functions.hpp"

#include <string>

using execution::ProgramContext;
using types::Type, types::Function, types::SkiddoObject;

SkiddoObject ConsFunction::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 2)
    throw TypeError(function_name + " takes 2 arguments but " + std::to_string(args.size()) + " were given");

  SkiddoObject first_arg = args[0].execute(ctx, {});
  SkiddoObject second_arg = args[1].execute(ctx, {});
  const std::vector<SkiddoObject>& items = second_arg.as_list()->items;

  std::vector<SkiddoObject> result = {first_arg};
  result.insert(result.end(), items.begin(), items.end());
  return SkiddoObject::list(result);
}

SkiddoObject HeadFunction::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 1)
    throw TypeError(function_name + " takes 1 argument but " + std::to_string(args.size()) + " were given");

  SkiddoObject executed = args[0].execute(ctx, {});
  const std::vector<SkiddoObject>& items = executed.as_list()->items;

  if (items.size() == 0) return SkiddoObject::literal();
  return items[0];
}

SkiddoObject TailFunction::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 1)
    throw TypeError(function_name + " takes 1 argument but " + std::to_string(args.size()) + " were given");

  SkiddoObject executed = args[0].execute(ctx, {});
  const std::vector<SkiddoObject>& items = executed.as_list()->items;

  if (items.size() == 0) return SkiddoObject::list({});
  return SkiddoObject::list({items.begin() + 1, items.end()});
}
