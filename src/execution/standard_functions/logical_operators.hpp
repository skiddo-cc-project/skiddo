#pragma once

#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>
#include <string>
#include <vector>

class UnaryLogicFunction : public types::Function {
 public:
  enum Op { NOT };

 private:
  Op type;
  bool perform_operation(bool value);

 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  UnaryLogicFunction(const std::string& function_name, const Op type) : types::Function(function_name), type(type) {}
};

class BinaryLogicFunction : public types::Function {
 public:
  enum Op { AND, OR, XOR };

 private:
  Op type;
  bool perform_operation(bool first, bool second);

 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  BinaryLogicFunction(const std::string& function_name, const Op type) : types::Function(function_name), type(type) {}
};
