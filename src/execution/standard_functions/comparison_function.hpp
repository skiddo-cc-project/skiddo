#pragma once

#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>
#include <string>
#include <vector>

/**
 * @brief The function evaluate its arguments and compare the results.
 *
 * After evaluation, the arguments should be of type integer, real, or boolean.
 * The functions perform usual comparisons and return a boolean value depending
 * on the result of comparison.
 */
class ComparisonFunction : public types::Function {
 public:
  enum Op { EQUAL, NONEQ, LESS, LESSEQ, GREATER, GREATEREQ };

 private:
  Op type;
  template <typename T>
  bool perform_comparison(T left, T right);

 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  ComparisonFunction(const std::string& function_name, const Op type) : types::Function(function_name), type(type) {}
};
