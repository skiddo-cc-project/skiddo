#pragma once

#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>
#include <vector>

/**
 * @brief Evaluates argument as a List and returns the first element or null, if the list is empty.
 */
class HeadFunction : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};

/**
 * @brief Evaluates argument as a List and returns a List of all elements without the first one (head).
 */
class TailFunction : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};

/**
 * @brief Constructs a new list adding its first argument as the first element to the list from the second argument.
 * After evaluation, the first argument can be of any type.
 * The second argument should be a list (perhaps, empty).
 * The function constructs a new list adding its first argument as the first element to the list from the second
 * argument. The function returns the list constructed
 */
class ConsFunction : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};
