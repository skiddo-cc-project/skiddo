#include "logical_operators.hpp"

using execution::ProgramContext;
using types::Type, types::Function, types::SkiddoObject, types::LiteralType;

bool UnaryLogicFunction::perform_operation(bool value) {
  switch (type) {
    case NOT:
      return !value;
  }
  throw FatalError("Unknown operator type");
}

SkiddoObject UnaryLogicFunction::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 1)
    throw TypeError(function_name + " takes 1 argument but " + std::to_string(args.size()) + " was given");

  const SkiddoObject obj = args[0].execute(ctx, {});
  const bool value = obj.as_literal()->as_bool()->value;
  return SkiddoObject::literal(perform_operation(value));
}

bool BinaryLogicFunction::perform_operation(bool first, bool second) {
  switch (type) {
    case AND:
      return first && second;
    case OR:
      return first || second;
    case XOR:
      return first ^ second;
  }
  throw FatalError("Unknown operator type");
}

SkiddoObject BinaryLogicFunction::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 2)
    throw TypeError(function_name + " takes 2 arguments but " + std::to_string(args.size()) + " was given");

  const SkiddoObject firstObj = args[0].execute(ctx, {});
  const SkiddoObject secondObj = args[1].execute(ctx, {});

  const bool firstBoolean = firstObj.as_literal()->as_bool()->value;
  const bool secondBoolean = secondObj.as_literal()->as_bool()->value;

  return SkiddoObject::literal(perform_operation(firstBoolean, secondBoolean));
}
