#include "evaluator.hpp"

using execution::ProgramContext;
using types::Type, types::Function, types::SkiddoObject;

SkiddoObject EvalFunction::_execute([[maybe_unused]] ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 1)
    throw TypeError("EvalFunction::execute takes 1 arguments but " + std::to_string(args.size()) + " was given");
  return args[0].execute(ctx, {});
}
