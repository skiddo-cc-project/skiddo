#pragma once

#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>
#include <string>
#include <vector>

/**
 * @brief The form introduces the standard arithmetic operations(Plus, Minus, Times, & Divide)
 *
 * The meaning of its parameters is the 2 operands, where the function
 * used is the operator between them, for example: plus x y is x + y
 * where the mathematical operations work same as in Python.
 */
class ArithmeticOperation : public types::Function {
 public:
  enum Op { PLUS, MINUS, TIMES, DIVIDE };

 private:
  Op type;
  template <typename T>
  T perform_operation(T left, T right);

 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  ArithmeticOperation(const std::string& function_name, const Op type) : types::Function(function_name), type(type) {}
};
