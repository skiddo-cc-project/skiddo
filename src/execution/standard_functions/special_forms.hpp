#pragma once

#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>
#include <vector>

/**
 * @brief The function just returns its argument without evaluating it.
 *
 * ( quote Element )
 *
 * The meaning of the function is to prevent evaluating its argument. Using the
 * single quote sign in front of an element is actually the short form of the
 * function.
 */
class QuoteFunction : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};

/**
 * @brief The function just prints the value of an argument
 *
 * ( print Element )
 */
class PrintFunction : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};

/**
 * @brief The form performs assigning a value to an atom.
 *
 * ( setq Atom Element )
 *
 * The second parameter gets evaluated, and the evaluated value becomes the new
 * value of the atom from the fist parameter replacing its previous value. If
 * there was not an atom with the given name in the current context then it is
 * created and added to the context. Otherwise, the atom that already exists in
 * the context gets the new value.
 */
class DefineFunction : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};

/**
 * @brief The form introduces a new user-defined function.
 *
 * The first argument becomes the name of the function. The second argument
 * should contain a number of atoms that represent the function parameters.
 * The third parameter of the form is considered as the body of the function.
 */
class CustomFunction : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};

/**
 * @brief The form introduces a new user-defined unnamed function.
 *
 * The meaning of its parameters is the same as for the second and the third
 * parameters of the func form. The unnamed function can further be called by
 * that name of an atom that gets it as the value, or directly.
 */
class LambdaFunction : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};

/**
 * @brief The form introduces a sequence of elements that are to be evaluated sequentially.
 *
 * The first parameter is the list of atoms that represent the local context of the form.
 * These atoms become known everywhere within the prog form and disappear after completing
 * its evaluation. The second argument contains elements that are to be evaluated sequentially.
 */
class ProgFunction : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};

/**
 * @brief The form introduces if else, without else if option.
 *
 * where the parameters are: first is the condition,
 * second is command to be executed if the condition is true,
 * and third parameter is optional and to be executed otherwise.
 * for example: cond true command1 command2,
 * which is same as if(true)command1; else command2;
 */
class CondFunction : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};

/**
 * @brief The form introduces while loop.
 *
 * The meaning of its parameters is: first parameter is the condition,
 * second parameter is the code/command to be executed while the condition is true.
 */

class While : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};

/**
 * @brief The form makes sense within a form that defines a local context (func, lambda or prog).
 *
 * It evaluates its argument and interrupts the execution of the nearest
 * enclosing form with the context. If there is no such enclosing form
 * then the whole program terminates.
 */
class ReturnFunction : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};

/*
 * @brief The form introduces break keyword in while loop.
 *
 * no parameters, you just type break inside the while loop to stop the loop,
 * if you typed it outside the while loop, nothing will happen.
 */
class Break : public types::Function {
 protected:
  virtual types::SkiddoObject _execute(execution::ProgramContext ctx, const std::vector<types::SkiddoObject>& args);

 public:
  using types::Function::Function;
};
