#pragma once

#include <skiddo/execution/execution.hpp>

namespace execution {

void init_standard_context(ProgramContext ctx);

}
