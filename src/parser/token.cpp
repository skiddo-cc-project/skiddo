#include <skiddo/parser/token.hpp>

const char* const parser::TokenNames[] = {
    "ENDMARKER", "NAME", "NULL", "BOOL", "INTEGER", "FLOAT", "LPAR", "RPAR", "QUOTE",
};
