#include <algorithm>
#include <cstring>
#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/parser/tokenizer.hpp>
#include <stdexcept>

#define is_potential_identifier_start(c) ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_')

#define is_potential_identifier_char(c) \
  ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '_')

#define is_potential_number_char(c) (c >= '0' && c <= '9')

using parser::Tokenizer, parser::Token, parser::string_true, parser::string_false, parser::string_null;

Tokenizer::Tokenizer(buffer::Buffer* buf) : buf(buf) { this->lineno = 0; }

Token Tokenizer::get(size_t* start, size_t* end) {
  int c;

  while (true) {
    /* Skip spaces and newlines */
    do {
      c = buf.nextc();
    } while (c == ' ' || c == '\t' || c == '\014' || c == '\n' || c == '\r');

    /* Check for EOF now */
    if (c == EOF) {
      *start = buf.cur;
      *end = buf.cur;
      return Token::ENDMARKER;
    }

    /* Skip single line comment */
    if (c == '#') {
      while (c != EOF && c != '\n') c = buf.nextc();
      continue;
    }
    /* Skip multiline comment */
    if (c == '/') {
      c = buf.nextc();
      if (c != '#') throw ErrorToken("There must be '#' behind the '/'");
      c = buf.nextc();
      int old_c = 0;
      while (old_c != '#' || c != '/') {
        old_c = c;
        c = buf.nextc();
        if (c == EOF) throw ErrorToken("Unexpected EOF inside multiline comment");
      }
      continue;
    }

    break;
  }

  size_t tkn_start = buf.cur - 1;

  /* Identifier (most frequent token!) */
  if (is_potential_identifier_start(c)) {
    bool is_true = true;
    bool is_false = true;
    bool is_null = true;
    size_t index = 0;

    while (is_potential_identifier_char(c)) {
      is_true = is_true && index < 4 && c == string_true[index];
      is_false = is_false && index < 5 && c == string_false[index];
      is_null = is_null && index < 4 && c == string_null[index];

      c = buf.nextc();
      index += 1;
    }
    buf.backup(c);

    is_true = is_true && index == 4;
    is_false = is_false && index == 5;
    is_null = is_null && index == 4;

    *start = tkn_start;
    *end = buf.cur;
    if (is_true || is_false) {
      return Token::BOOL;
    } else if (is_null) {
      return Token::NIL;
    } else {
      return Token::NAME;
    }
  }

  /* Number */
  if (is_potential_number_char(c) || c == '-' || c == '+') {
    bool found_decimal_separator = false;

    if (c == '-' || c == '+') {
      c = buf.nextc();
      if (!is_potential_number_char(c)) {
        throw SyntaxError("Invalid syntax");
      }
    }

    if (c == '0') {
      c = buf.nextc();
      if (c == '.') {
        found_decimal_separator = true;
        c = buf.nextc();
        if (!is_potential_number_char(c)) {
          throw SyntaxError("Invalid syntax");
        }
      } else if (is_potential_number_char(c)) {
        throw SyntaxError("Leading zeros in decimal literals are not permitted");
      }
    }

    while (is_potential_number_char(c) || c == '.') {
      if (c == '.') {
        if (!found_decimal_separator) {
          c = buf.nextc();
          if (!is_potential_number_char(c)) {
            throw SyntaxError("Invalid syntax");
          }
          found_decimal_separator = true;
        } else {
          throw SyntaxError("Invalid literal");
        }
      }
      c = buf.nextc();
    }

    if (is_potential_identifier_start(c) || c == '-' || c == '+') {
      throw SyntaxError("Invalid literal");
    }

    buf.backup(c);

    *start = tkn_start;
    *end = buf.cur;
    if (found_decimal_separator) {
      return Token::FLOAT;
    }
    return Token::INTEGER;
  }

  /* Quote */
  if (c == '\'') {
    *start = tkn_start;
    *end = buf.cur;
    return Token::QUOTE;
  }

  /* Keep track of parentheses nesting level */
  if (c == '(' || c == ')') {
    *start = tkn_start;
    *end = buf.cur;
    return c == '(' ? Token::LPAR : Token::RPAR;
  }
  throw SyntaxError("Unknown token");
}
