#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/parser/ast_builder.hpp>
#include <string>
#include <vector>

using parser::ASTBuilder, parser::Token;
using types::SkiddoObject;

types::SkiddoObject ASTBuilder::parse_any(parser::Token t, size_t start, size_t end) {
  if (t == Token::LPAR) return parse_list(t, start, end);
  if (t == Token::NAME) return parse_atom(t, start, end);
  if (t == Token::INTEGER || t == Token::FLOAT) return parse_number(t, start, end);
  if (t == Token::BOOL) return parse_bool(t, start, end);
  if (t == Token::NIL) return parse_null(t, start, end);
  if (t == Token::QUOTE) return parse_quote(t, start, end);

  throw SyntaxError("Unexpected token " + std::string(TokenNames[t]));
}

SkiddoObject ASTBuilder::parse_atom(Token t, size_t start, size_t end) {
  if (t != Token::NAME) throw SyntaxError("Expected token NAME, but found " + std::string(TokenNames[t]));
  return SkiddoObject::atom(buffer->get(start, end));
}

SkiddoObject ASTBuilder::parse_number(Token t, size_t start, size_t end) {
  if (t != Token::INTEGER && t != Token::FLOAT)
    throw SyntaxError("Expected token INTEGER or FLOAT, but found " + std::string(TokenNames[t]));
  std::string s = buffer->get(start, end);

  try {
    if (t == Token::INTEGER)
      return SkiddoObject::literal((int64_t)std::stoll(s));
    else
      return SkiddoObject::literal(std::stod(s));
  } catch (std::invalid_argument& e) {
    throw FatalError("Invalid literal " + s);
  }
}

SkiddoObject ASTBuilder::parse_bool(parser::Token t, size_t start, size_t end) {
  if (t != Token::BOOL) throw SyntaxError("Expected token BOOL, but found " + std::string(TokenNames[t]));
  std::string s = buffer->get(start, end);

  if (s == "true") {
    return SkiddoObject::literal(true);
  } else if (s == "false") {
    return SkiddoObject::literal(false);
  } else {
    throw FatalError("Invalid literal " + s);
  }
}

SkiddoObject ASTBuilder::parse_null(parser::Token t, size_t start, size_t end) {
  if (t != Token::NIL) throw SyntaxError("Expected token NULL, but found " + std::string(TokenNames[t]));
  std::string s = buffer->get(start, end);
  if (s != "null") throw FatalError("Invalid literal " + s);
  return SkiddoObject::literal();
}

SkiddoObject ASTBuilder::parse_quote(parser::Token t, size_t start, size_t end) {
  if (t != Token::QUOTE) throw SyntaxError("Expected token QUOTE, but found " + std::string(TokenNames[t]));

  t = tokenizer->get(&start, &end);
  return SkiddoObject::quote(parse_any(t, start, end));
}

SkiddoObject ASTBuilder::parse_list(Token t, size_t start, size_t end) {
  if (t != Token::LPAR) throw SyntaxError("Expected token LPAR, but found " + std::string(TokenNames[t]));

  std::vector<SkiddoObject> items;

  t = tokenizer->get(&start, &end);
  while (t != Token::RPAR) {
    items.push_back(parse_any(t, start, end));
    t = tokenizer->get(&start, &end);
  }

  return SkiddoObject::list(items);
}

SkiddoObject ASTBuilder::get() {
  size_t start, end;
  Token t = tokenizer->get(&start, &end);
  if (t == Token::ENDMARKER) throw EndMarkerToken();
  return parse_any(t, start, end);
}
