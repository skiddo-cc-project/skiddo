#include <cstring>
#include <skiddo/buffer/buffer.hpp>
#include <stdexcept>

#include "macro.hpp"

using buffer::Buffer;

Buffer::~Buffer() {
  if (this->buf != NULL) free(this->buf);
}

void Buffer::reserve(size_t size) {
  if (this->buf == NULL) {
    size_t new_size = size + (size >> 1);
    this->buf = reinterpret_cast<char*>(malloc(new_size));
    this->inp = this->buf;
    this->end = this->buf + new_size;
    return;
  }

  size_t free_space = this->end - this->inp;
  if (size <= free_space) return;  // Has enough space

  // Allocate more memory
  size_t data_size = this->inp - this->buf;
  size_t new_size = data_size + std::max(size, data_size >> 1);
  this->buf = reinterpret_cast<char*>(realloc(this->buf, new_size));
  if (this->buf == NULL) throw std::runtime_error("Buffer::reserve(): Out of memory exception");
  this->inp = this->buf + data_size;
  this->end = this->buf + new_size;
}

void Buffer::push(const char* data, size_t size) {
  this->reserve(size);
  std::memcpy(this->inp, data, size);
  this->inp += size;
}
