#include <skiddo/types/enums.hpp>

const char* types::literalToString(types::LiteralType type) {
  switch (type) {
    case types::LiteralType::LITERAL_NULL:
      return "null";
      break;
    case types::LiteralType::LITERAL_INT:
      return "int";
      break;
    case types::LiteralType::LITERAL_FLOAT:
      return "float";
      break;
    case types::LiteralType::LITERAL_BOOL:
      return "bool";
      break;
    default:
      return "unknown";
  }
}
