#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>
#include <string>

using execution::ProgramContext;
using types::Atom, types::Literal, types::List, types::Quote;
using types::Function, types::SkiddoObject;

SkiddoObject Atom::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 0)
    throw TypeError(function_name + " takes 0 arguments but " + std::to_string(args.size()) + " was given");
  return ctx.get_func(name);
}

SkiddoObject Literal::_execute([[maybe_unused]] ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 0)
    throw TypeError(function_name + " takes 0 arguments but " + std::to_string(args.size()) + " was given");
  return to_object();
}

types::LiteralNull* Literal::as_null() {
  if (get_literal_type() != LiteralType::LITERAL_NULL)
    throw TypeError("'" + to_str() + "'  Literal cannot be interpreted as a LiteralNull");
  return (types::LiteralNull*)this;
}

types::LiteralBool* Literal::as_bool() {
  if (get_literal_type() != LiteralType::LITERAL_BOOL)
    throw TypeError("'" + to_str() + "'  Literal cannot be interpreted as a LiteralBool");
  return (types::LiteralBool*)this;
}

types::LiteralInt* Literal::as_int() {
  if (get_literal_type() != LiteralType::LITERAL_INT)
    throw TypeError("'" + to_str() + "'  Literal cannot be interpreted as a LiteralInt");
  return (types::LiteralInt*)this;
}

types::LiteralFloat* Literal::as_float() {
  if (get_literal_type() != LiteralType::LITERAL_FLOAT)
    throw TypeError("'" + to_str() + "'  Literal cannot be interpreted as a LiteralFloat");
  return (types::LiteralFloat*)this;
}

SkiddoObject List::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 0)
    throw TypeError(function_name + " takes 0 arguments but " + std::to_string(args.size()) + " was given");
  if (items.size() < 1) throw TypeError("the List to be launched must have at least one item but it has 0 items");

  return items[0].execute(ctx, {}).execute(ctx, std::vector<SkiddoObject>(items.begin() + 1, items.end()));
}

SkiddoObject Quote::_execute([[maybe_unused]] ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != 0)
    throw TypeError(function_name + " takes 0 arguments but " + std::to_string(args.size()) + " was given");
  return item;
}
