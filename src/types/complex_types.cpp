#include <memory>
#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/complex_types.hpp>
#include <string>

using execution::ProgramContext;
using types::FunctionWithContext, types::SkiddoObject;

SkiddoObject FunctionWithContext::_execute(ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  if (args.size() != local_vars.size())
    throw TypeError(function_name + " takes " + std::to_string(local_vars.size()) + " arguments but " +
                    std::to_string(args.size()) + " was given");

  ProgramContext new_ctx = ctx.new_local_state();

  for (uint i = 0; i < local_vars.size(); i++) new_ctx.register_func(local_vars[i], args[i].execute(ctx, {}), true);

  try {
    SkiddoObject last_res = SkiddoObject::literal();
    for (const SkiddoObject& obj : body) {
      last_res = obj.execute(new_ctx, {});
    }
    return last_res;
  } catch (ReturnException& e) {
    return e.getPayload();
  }
}
