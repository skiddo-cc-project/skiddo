#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>
#include <string>

using execution::ProgramContext;
using types::Atom, types::Literal, types::List, types::Quote, types::FunctionWithContext;
using types::Function, types::SkiddoObject;

SkiddoObject Function::execute(execution::ProgramContext ctx, const std::vector<SkiddoObject>& args) {
  execution::ProgramState& state = *ctx.get_state();
  if (state.has_keyboard_interrupt) throw KeyboardInterrupt();

  const size_t tracebackSize = state.traceback.size();
  state.traceback.push_back(to_object());

  SkiddoObject res;
  try {
    res = _execute(ctx, args);
  } catch (SystemError& e) {
    state.traceback.pop_back();
    throw;
  }

  state.traceback.pop_back();
  if (state.traceback.size() != tracebackSize)
    throw FatalError("traceback size before and after the object call is not the same");
  return res;
}

SkiddoObject Function::to_object() { return SkiddoObject(self.lock()); }

SkiddoObject SkiddoObject::nothing() { return SkiddoObject(new LiteralNull(false)); }
SkiddoObject SkiddoObject::atom(const std::string& name) { return SkiddoObject(new Atom(name)); }
SkiddoObject SkiddoObject::list(const std::vector<SkiddoObject>& items) { return SkiddoObject(new List(items)); }
SkiddoObject SkiddoObject::quote(const SkiddoObject& item) { return SkiddoObject(new Quote(item)); }
SkiddoObject SkiddoObject::literal() { return SkiddoObject(new LiteralNull(true)); }
SkiddoObject SkiddoObject::literal(const bool& value) { return SkiddoObject(new LiteralBool(value)); }
SkiddoObject SkiddoObject::literal(const int64_t& value) { return SkiddoObject(new LiteralInt(value)); }
SkiddoObject SkiddoObject::literal(const double& value) { return SkiddoObject(new LiteralFloat(value)); }

Atom* SkiddoObject::as_atom() const {
  if (get_type() != Type::ATOM) throw TypeError("'" + to_str() + "' object cannot be interpreted as an Atom");
  return reinterpret_cast<Atom*>(get());
}

Literal* SkiddoObject::as_literal() const {
  if (get_type() != Type::LITERAL) throw TypeError("'" + to_str() + "' object cannot be interpreted as a Literal");
  return reinterpret_cast<Literal*>(get());
}

List* SkiddoObject::as_list() const {
  if (get_type() != Type::LIST) throw TypeError("'" + to_str() + "' object cannot be interpreted as a List");
  return reinterpret_cast<List*>(get());
}

Quote* SkiddoObject::as_quote() const {
  if (get_type() != Type::QUOTE) throw TypeError("'" + to_str() + "' object cannot be interpreted as a Quote");
  return reinterpret_cast<Quote*>(get());
}

SkiddoObject SkiddoObject::execute(execution::ProgramContext ctx, const std::vector<SkiddoObject>& args) const {
  return (*this)->execute(ctx, args);
}
