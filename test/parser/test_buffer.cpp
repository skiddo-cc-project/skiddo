#include <gtest/gtest.h>

#include <skiddo/buffer/buffer.hpp>
#include <skiddo/parser/tokenizer.hpp>

TEST(BefferTest, PushAndMeasureSize) {
  buffer::Buffer buf;
  EXPECT_EQ(buf.size(), 0);
  buf.push("abc");
  EXPECT_EQ(buf.size(), 3);
  buf.push("abcdef");
  EXPECT_EQ(buf.size(), 9);
}

TEST(BefferTest, PushAndGet) {
  buffer::Buffer buf;
  buf.push("abc");
  EXPECT_EQ(*buf.get(2), 'c');
  EXPECT_EQ(*buf.get(0), 'a');
  buf.push("abcdef");
  EXPECT_EQ(*buf.get(0), 'a');
  EXPECT_EQ(*(buf.get(0) + 3), 'a');
  EXPECT_EQ(*(buf.get(1) + 3), 'b');
  EXPECT_EQ(*buf.get(8), 'f');
}
