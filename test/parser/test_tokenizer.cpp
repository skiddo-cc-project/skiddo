#include <gtest/gtest.h>

#include <skiddo/buffer/buffer.hpp>
#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/parser/tokenizer.hpp>

TEST(TokenizerTest, EmptyProgram) {
  buffer::Buffer buf("");
  parser::Tokenizer tokenizer(&buf);

  for (int i = 0; i < 10; i++) {
    size_t start = 0, end = 0;
    parser::Token t = tokenizer.get(&start, &end);
    EXPECT_EQ(t, parser::Token::ENDMARKER);
  }
}

TEST(TokenizerTest, SimpleProgram) {
  buffer::Buffer buf("(setq f a5)\nx\n");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token LPAR (from 0 to 1): '('
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::LPAR);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 1);

  // Token NAME (from 1 to 5): 'setq'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 1);
  EXPECT_EQ(end, 5);

  // Token NAME (from 6 to 7): 'f'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 6);
  EXPECT_EQ(end, 7);

  // Token NAME (from 8 to 10): 'a5'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 8);
  EXPECT_EQ(end, 10);

  // Token RPAR (from 10 to 11): ')'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::RPAR);
  EXPECT_EQ(start, 10);
  EXPECT_EQ(end, 11);

  // Token NAME (from 12 to 13): 'x'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 12);
  EXPECT_EQ(end, 13);

  // Token ENDMARKER (from 14 to 14): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 14);
  EXPECT_EQ(end, 14);
}

TEST(TokenizerTest, OneLineComments) {
  buffer::Buffer buf("a# 1comment\n#comment2 x\nxa#\n#### /#");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token NAME (from 0 to 1): 'a'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 1);

  // Token NAME (from 24 to 26): 'xa'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 24);
  EXPECT_EQ(end, 26);

  // Token ENDMARKER (from 35 to 35): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 35);
  EXPECT_EQ(end, 35);
}

TEST(TokenizerTest, MultilineComments) {
  buffer::Buffer buf("a/##/b/#\nx\n/# /#\n#//##/c");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token NAME (from 0 to 1): 'a'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 1);

  // Token NAME (from 5 to 6): 'b'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 5);
  EXPECT_EQ(end, 6);

  // Token NAME (from 23 to 24): 'c'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 23);
  EXPECT_EQ(end, 24);

  // Token ENDMARKER (from 24 to 24): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 24);
  EXPECT_EQ(end, 24);
}

TEST(TokenizerTest, IncorrectMultilineComments1) {
  buffer::Buffer buf("a/#/b");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token NAME (from 0 to 1): 'a'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 1);

  EXPECT_THROW(
      {
        try {
          tokenizer.get(&start, &end);
        } catch (const ErrorToken& e) {
          EXPECT_STREQ("SyntaxError: Unexpected EOF inside multiline comment", e.what());
          throw;
        }
      },
      ErrorToken);
}

TEST(TokenizerTest, ZeroNumber) {
  buffer::Buffer buf("0");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token NUMBER (from 0 to 1): '0'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::INTEGER);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 1);

  // Token ENDMARKER (from 1 to 1): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 1);
  EXPECT_EQ(end, 1);
}

TEST(TokenizerTest, SingleDigitIntegerNumber) {
  buffer::Buffer buf("7");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token NUMBER (from 0 to 1): '7'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::INTEGER);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 1);

  // Token ENDMARKER (from 1 to 1): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 1);
  EXPECT_EQ(end, 1);
}

TEST(TokenizerTest, IntegerNumber) {
  buffer::Buffer buf("42");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token NUMBER (from 0 to 1): '42'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::INTEGER);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 2);

  // Token ENDMARKER (from 2 to 2): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 2);
  EXPECT_EQ(end, 2);
}

TEST(TokenizerTest, ZeroFloatingNumber) {
  buffer::Buffer buf("0.159");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token NUMBER (from 0 to 5): '0.159'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::FLOAT);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 5);

  // Token ENDMARKER (from 5 to 5): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 5);
  EXPECT_EQ(end, 5);
}

TEST(TokenizerTest, OneDigitFloatingNumber) {
  buffer::Buffer buf("3.14");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token NUMBER (from 0 to 4): '3.14'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::FLOAT);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 4);

  // Token ENDMARKER (from 4 to 4): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 4);
  EXPECT_EQ(end, 4);
}

TEST(TokenizerTest, FloatingNumber) {
  buffer::Buffer buf("125.41");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token NUMBER (from 0 to 6): '125.41'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::FLOAT);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 6);

  // Token ENDMARKER (from 6 to 6): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 6);
  EXPECT_EQ(end, 6);
}

TEST(TokenizerTest, InvalidZeroFloatingNumber) {
  buffer::Buffer buf("0.");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  EXPECT_THROW(
      {
        try {
          tokenizer.get(&start, &end);
        } catch (const SyntaxError& e) {
          EXPECT_STREQ("SyntaxError: Invalid syntax", e.what());
          throw;
        }
      },
      SyntaxError);
}

TEST(TokenizerTest, LeadingZeroInteger) {
  buffer::Buffer buf("0000149");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  EXPECT_THROW(
      {
        try {
          tokenizer.get(&start, &end);
        } catch (const SyntaxError& e) {
          EXPECT_STREQ("SyntaxError: Leading zeros in decimal literals are not permitted", e.what());
          throw;
        }
      },
      SyntaxError);
}

TEST(TokenizerTest, InvalidFloatingNumber) {
  buffer::Buffer buf("1.");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  EXPECT_THROW(
      {
        try {
          tokenizer.get(&start, &end);
        } catch (const SyntaxError& e) {
          EXPECT_STREQ("SyntaxError: Invalid syntax", e.what());
          throw;
        }
      },
      SyntaxError);
}

TEST(TokenizerTest, InvalidFloatingSeparator) {
  buffer::Buffer buf("43.1233.210");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  EXPECT_THROW(
      {
        try {
          tokenizer.get(&start, &end);
        } catch (const SyntaxError& e) {
          EXPECT_STREQ("SyntaxError: Invalid literal", e.what());
          throw;
        }
      },
      SyntaxError);
}

TEST(TokenizerTest, InvalidNumberLiteral) {
  buffer::Buffer buf("(setq x 5a4)");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token LPAR (from 0 to 1): '('
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::LPAR);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 1);

  // Token NAME (from 1 to 5): 'setq'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 1);
  EXPECT_EQ(end, 5);

  // Token NAME (from 6 to 7): 'x'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 6);
  EXPECT_EQ(end, 7);

  EXPECT_THROW(
      {
        try {
          tokenizer.get(&start, &end);
        } catch (const SyntaxError& e) {
          EXPECT_STREQ("SyntaxError: Invalid literal", e.what());
          throw;
        }
      },
      SyntaxError);
}

TEST(TokenizerTest, QuoteLiteral) {
  buffer::Buffer buf("'abcd");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token QUOTE (from 0 to 1): '''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::QUOTE);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 1);

  // Token NAME (from 1 to 5): 'abcd'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 1);
  EXPECT_EQ(end, 5);

  // Token ENDMARKER (from 5 to 5): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 5);
  EXPECT_EQ(end, 5);
}

TEST(TokenizerTest, QuoteLiteralComplex) {
  buffer::Buffer buf("(quote '' ' ('abcd))");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token LPAR (from 0 to 1): '('
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::LPAR);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 1);

  // Token NAME (from 1 to 6): 'quote'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 1);
  EXPECT_EQ(end, 6);

  // Token QUOTE (from 7 to 8): '''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::QUOTE);
  EXPECT_EQ(start, 7);
  EXPECT_EQ(end, 8);

  // Token QUOTE (from 8 to 9): '''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::QUOTE);
  EXPECT_EQ(start, 8);
  EXPECT_EQ(end, 9);

  // Token QUOTE (from 10 to 11): '''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::QUOTE);
  EXPECT_EQ(start, 10);
  EXPECT_EQ(end, 11);

  // Token LPAR (from 12 to 13): '('
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::LPAR);
  EXPECT_EQ(start, 12);
  EXPECT_EQ(end, 13);

  // Token QUOTE (from 13 to 14): '''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::QUOTE);
  EXPECT_EQ(start, 13);
  EXPECT_EQ(end, 14);

  // Token NAME (from 14 to 18): 'abcd'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 14);
  EXPECT_EQ(end, 18);

  // Token RPAR (from 18 to 19): ')'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::RPAR);
  EXPECT_EQ(start, 18);
  EXPECT_EQ(end, 19);

  // Token RPAR (from 19 to 20): ')'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::RPAR);
  EXPECT_EQ(start, 19);
  EXPECT_EQ(end, 20);

  // Token ENDMARKER (from 20 to 20): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 20);
  EXPECT_EQ(end, 20);
}

TEST(TokenizerTest, QuoteLiteralArguments) {
  buffer::Buffer buf("(plus '1'2)");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token LPAR (from 0 to 1): '('
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::LPAR);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 1);

  // Token NAME (from 1 to 5): 'plus'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 1);
  EXPECT_EQ(end, 5);

  // Token QUOTE (from 6 to 7): '''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::QUOTE);
  EXPECT_EQ(start, 6);
  EXPECT_EQ(end, 7);

  // Token NUMBER (from 7 to 8): '1'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::INTEGER);
  EXPECT_EQ(start, 7);
  EXPECT_EQ(end, 8);

  // Token QUOTE (from 8 to 9): '''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::QUOTE);
  EXPECT_EQ(start, 8);
  EXPECT_EQ(end, 9);

  // Token NUMBER (from 9 to 10): '2'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::INTEGER);
  EXPECT_EQ(start, 9);
  EXPECT_EQ(end, 10);

  // Token RPAR (from 10 to 11): ')'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::RPAR);
  EXPECT_EQ(start, 10);
  EXPECT_EQ(end, 11);

  // Token ENDMARKER (from 11 to 11): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 11);
  EXPECT_EQ(end, 11);
}

TEST(TokenizerTest, Booleans) {
  buffer::Buffer buf("(or true false)");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token LPAR (from 0 to 1): '('
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::LPAR);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 1);

  // Token NAME (from 1 to 3): 'or'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 1);
  EXPECT_EQ(end, 3);

  // Token BOOL (from 4 to 8): 'true'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::BOOL);
  EXPECT_EQ(start, 4);
  EXPECT_EQ(end, 8);

  // Token BOOL (from 9 to 14): 'false'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::BOOL);
  EXPECT_EQ(start, 9);
  EXPECT_EQ(end, 14);

  // Token RPAR (from 14 to 15): ')'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::RPAR);
  EXPECT_EQ(start, 14);
  EXPECT_EQ(end, 15);

  // Token ENDMARKER (from 15 to 15): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 15);
  EXPECT_EQ(end, 15);
}

TEST(TokenizerTest, Null) {
  buffer::Buffer buf("(setq x null)");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token LPAR (from 0 to 1): '('
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::LPAR);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 1);

  // Token NAME (from 1 to 5): 'setq'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 1);
  EXPECT_EQ(end, 5);

  // Token NAME (from 6 to 7): 'x'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 6);
  EXPECT_EQ(end, 7);

  // Token NULL (from 8 to 12): 'null'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NIL);
  EXPECT_EQ(start, 8);
  EXPECT_EQ(end, 12);

  // Token RPAR (from 12 to 13): ')'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::RPAR);
  EXPECT_EQ(start, 12);
  EXPECT_EQ(end, 13);

  // Token ENDMARKER (from 13 to 13): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 13);
  EXPECT_EQ(end, 13);
}

TEST(TokenizerTest, NullMixed) {
  buffer::Buffer buf("(setq x (cond true 'one null))");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token LPAR (from 0 to 1): '('
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::LPAR);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 1);

  // Token NAME (from 1 to 5): 'setq'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 1);
  EXPECT_EQ(end, 5);

  // Token NAME (from 6 to 7): 'x'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 6);
  EXPECT_EQ(end, 7);

  // Token LPAR (from 8 to 9): '('
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::LPAR);
  EXPECT_EQ(start, 8);
  EXPECT_EQ(end, 9);

  // Token NAME (from 9 to 13): 'cond'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 9);
  EXPECT_EQ(end, 13);

  // Token BOOL (from 14 to 18): 'true'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::BOOL);
  EXPECT_EQ(start, 14);
  EXPECT_EQ(end, 18);

  // Token QUOTE (from 19 to 20): '''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::QUOTE);
  EXPECT_EQ(start, 19);
  EXPECT_EQ(end, 20);

  // Token NAME (from 20 to 23): 'one'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NAME);
  EXPECT_EQ(start, 20);
  EXPECT_EQ(end, 23);

  // Token NULL (from 24 to 28): 'null'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::NIL);
  EXPECT_EQ(start, 24);
  EXPECT_EQ(end, 28);

  // Token RPAR (from 28 to 29): ')'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::RPAR);
  EXPECT_EQ(start, 28);
  EXPECT_EQ(end, 29);

  // Token RPAR (from 29 to 30): ')'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::RPAR);
  EXPECT_EQ(start, 29);
  EXPECT_EQ(end, 30);

  // Token ENDMARKER (from 30 to 30): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 30);
  EXPECT_EQ(end, 30);
}

TEST(TokenizerTest, IntegerWithPlusSign) {
  buffer::Buffer buf("+7");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token NUMBER (from 0 to 2): '+7'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::INTEGER);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 2);

  // Token ENDMARKER (from 2 to 2): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 2);
  EXPECT_EQ(end, 2);
}

TEST(TokenizerTest, IntegerWithMinusSign) {
  buffer::Buffer buf("-7");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token NUMBER (from 0 to 2): '-7'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::INTEGER);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 2);

  // Token ENDMARKER (from 2 to 2): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 2);
  EXPECT_EQ(end, 2);
}

TEST(TokenizerTest, IntegerWithTwoPlusSigns) {
  buffer::Buffer buf("++7");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  EXPECT_THROW(
      {
        try {
          tokenizer.get(&start, &end);
        } catch (const SyntaxError& e) {
          EXPECT_STREQ("SyntaxError: Invalid syntax", e.what());
          throw;
        }
      },
      SyntaxError);
}

TEST(TokenizerTest, IntegerWithTwoMinusSigns) {
  buffer::Buffer buf("--7");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  EXPECT_THROW(
      {
        try {
          tokenizer.get(&start, &end);
        } catch (const SyntaxError& e) {
          EXPECT_STREQ("SyntaxError: Invalid syntax", e.what());
          throw;
        }
      },
      SyntaxError);
}

TEST(TokenizerTest, FloatWithTwoZeroes) {
  buffer::Buffer buf("00.7");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  EXPECT_THROW(
      {
        try {
          tokenizer.get(&start, &end);
        } catch (const SyntaxError& e) {
          EXPECT_STREQ("SyntaxError: Leading zeros in decimal literals are not permitted", e.what());
          throw;
        }
      },
      SyntaxError);
}

TEST(TokenizerTest, FloatWithTwoZeroesAndPlus) {
  buffer::Buffer buf("+00.7");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  EXPECT_THROW(
      {
        try {
          tokenizer.get(&start, &end);
        } catch (const SyntaxError& e) {
          EXPECT_STREQ("SyntaxError: Leading zeros in decimal literals are not permitted", e.what());
          throw;
        }
      },
      SyntaxError);
}

TEST(TokenizerTest, FloatWithPlusSign) {
  buffer::Buffer buf("+1.7");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token NUMBER (from 0 to 4): '+1.7'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::FLOAT);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 4);

  // Token ENDMARKER (from 4 to 4): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 4);
  EXPECT_EQ(end, 4);
}

TEST(TokenizerTest, FloatWithMinusSign) {
  buffer::Buffer buf("-0.7");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  // Token NUMBER (from 0 to 4): '-0.7'
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::FLOAT);
  EXPECT_EQ(start, 0);
  EXPECT_EQ(end, 4);

  // Token ENDMARKER (from 4 to 4): ''
  EXPECT_EQ(tokenizer.get(&start, &end), parser::Token::ENDMARKER);
  EXPECT_EQ(start, 4);
  EXPECT_EQ(end, 4);
}

TEST(TokenizerTest, NumberMinusNumber) {
  buffer::Buffer buf("1-2");
  parser::Tokenizer tokenizer(&buf);

  size_t start = 0, end = 0;

  EXPECT_THROW(
      {
        try {
          tokenizer.get(&start, &end);
        } catch (const SyntaxError& e) {
          EXPECT_STREQ("SyntaxError: Invalid literal", e.what());
          throw;
        }
      },
      SyntaxError);
}
