#include <gtest/gtest.h>

#include <memory>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>

using execution::ProgramContext;
using types::SkiddoObject, types::Type, types::LiteralType;

TEST(PrintTest, SimpleSetQ) {
  SkiddoObject res =
      SkiddoObject::list({SkiddoObject::atom("setq"), SkiddoObject::atom("x"), SkiddoObject::literal(3.4)});

  const std::string& text = res.to_str();

  EXPECT_STREQ(text.c_str(), "(setq x 3.4)");
}
