#include <gtest/gtest.h>

#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>

using execution::ProgramContext;
using types::SkiddoObject, types::Type, types::LiteralType;

class ArithmeticBinaryFunctionsTests
    // <function_name, left_operand, right_operand, expected_result>
    : public ::testing::TestWithParam<std::tuple<std::string, SkiddoObject, SkiddoObject, SkiddoObject>> {};

TEST_P(ArithmeticBinaryFunctionsTests, NormalCase) {
  ProgramContext ctx = ProgramContext::standard_state();

  std::string function_name = std::get<0>(GetParam());
  SkiddoObject left = std::get<1>(GetParam());
  SkiddoObject right = std::get<2>(GetParam());
  SkiddoObject expectedResult = std::get<3>(GetParam());

  SkiddoObject func = ctx.get_func(function_name);
  SkiddoObject res = func.execute(ctx, {left, right});
  ASSERT_EQ(res.as_literal()->get_literal_type(), expectedResult.as_literal()->get_literal_type());
  EXPECT_DOUBLE_EQ(res.as_literal()->convertValueTo<double>(), expectedResult.as_literal()->convertValueTo<double>());
}

INSTANTIATE_TEST_SUITE_P(
    ArithmeticOperatorsTest, ArithmeticBinaryFunctionsTests,
    testing::Values(
        std::make_tuple("plus", SkiddoObject::literal(2), SkiddoObject::literal(4), SkiddoObject::literal(6)),
        std::make_tuple("plus", SkiddoObject::literal(2), SkiddoObject::literal(-4), SkiddoObject::literal(-2)),
        std::make_tuple("minus", SkiddoObject::literal(2), SkiddoObject::literal(3), SkiddoObject::literal(-1)),
        std::make_tuple("times", SkiddoObject::literal(2), SkiddoObject::literal(4), SkiddoObject::literal(8)),
        std::make_tuple("times", SkiddoObject::literal(1.5), SkiddoObject::literal(4), SkiddoObject::literal(6.0)),
        std::make_tuple("divide", SkiddoObject::literal(3), SkiddoObject::literal(2), SkiddoObject::literal(1.5)),
        std::make_tuple("divide", SkiddoObject::literal(4), SkiddoObject::literal(2), SkiddoObject::literal(2.0))));
