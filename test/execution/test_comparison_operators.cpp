#include <gtest/gtest.h>

#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>
#include <src/execution/standard_functions/standard_functions.hpp>
#include <string>
#include <tuple>

using execution::ProgramContext;
using types::SkiddoObject, types::Type, types::LiteralType;

class ComparisonFunctionTest
    // <function_name, left_operand, right_operand, expected_result>
    : public ::testing::TestWithParam<std::tuple<std::string, SkiddoObject, SkiddoObject, bool>> {};

TEST_P(ComparisonFunctionTest, CheckComparisonFunction) {
  ProgramContext ctx = ProgramContext::standard_state();

  std::string function_name = std::get<0>(GetParam());
  SkiddoObject left = std::get<1>(GetParam());
  SkiddoObject right = std::get<2>(GetParam());
  bool expectedResult = std::get<3>(GetParam());

  SkiddoObject func = ctx.get_func(function_name);
  SkiddoObject result = func.execute(ctx, {left, right});
  EXPECT_EQ(result.as_literal()->as_bool()->value, expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    ComparisonFunctionTests, ComparisonFunctionTest,
    testing::Values(std::make_tuple("equal", SkiddoObject::literal(3.14), SkiddoObject::literal(3.14), true),
                    std::make_tuple("less", SkiddoObject::literal(5.0), SkiddoObject::literal(5), false),
                    std::make_tuple("nonequal", SkiddoObject::literal(1.0), SkiddoObject::literal(true), false),
                    std::make_tuple("greatereq", SkiddoObject::literal(7), SkiddoObject::literal(7.0), true),
                    std::make_tuple("equal", SkiddoObject::literal(true), SkiddoObject::literal(1), true),
                    std::make_tuple("nonequal", SkiddoObject::literal(4), SkiddoObject::literal(), true),
                    std::make_tuple("equal", SkiddoObject::literal(), SkiddoObject::literal(1.56), false),
                    std::make_tuple("equal", SkiddoObject::literal(1), SkiddoObject::literal(true), true),
                    std::make_tuple("equal", SkiddoObject::literal(true), SkiddoObject::literal(true), true),
                    std::make_tuple("lesseq", SkiddoObject::literal(10), SkiddoObject::literal(12.0), true),
                    std::make_tuple("equal", SkiddoObject::literal(false), SkiddoObject::literal(0), true),
                    std::make_tuple("equal", SkiddoObject::literal(0), SkiddoObject::literal(5), false),
                    std::make_tuple("nonequal", SkiddoObject::literal(3), SkiddoObject::literal(12), true),
                    std::make_tuple("less", SkiddoObject::literal(3), SkiddoObject::literal(12), true),
                    std::make_tuple("greater", SkiddoObject::literal(4), SkiddoObject::literal(125), false),
                    std::make_tuple("greater", SkiddoObject::literal(4), SkiddoObject::literal(3), true),
                    std::make_tuple("nonequal", SkiddoObject::literal(false), SkiddoObject::literal(true), true),
                    std::make_tuple("lesseq", SkiddoObject::literal(true), SkiddoObject::literal(false), false),
                    std::make_tuple("greater", SkiddoObject::literal(true), SkiddoObject::literal(false), true),
                    std::make_tuple("greatereq", SkiddoObject::literal(true), SkiddoObject::literal(true), true),
                    std::make_tuple("lesseq", SkiddoObject::literal(5.0), SkiddoObject::literal(6.3), true),
                    std::make_tuple("greater", SkiddoObject::literal(7.22), SkiddoObject::literal(14.0), false),
                    std::make_tuple("greatereq", SkiddoObject::literal(14.0), SkiddoObject::literal(14.0), true)));

TEST(ComparisonTest, ExpectTwoArguments) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("equal");

  EXPECT_THROW(
      {
        try {
          SkiddoObject result = func.execute(ctx, {
                                                      SkiddoObject::literal(false),
                                                      SkiddoObject::literal(0),
                                                      SkiddoObject::literal(5),
                                                  });
        } catch (const TypeError& e) {
          EXPECT_STREQ("TypeError: equal takes 2 arguments but 3 was given", e.what());
          throw;
        }
      },
      TypeError);
}

TEST(ComparisonTest, ExpectFirstAsLiteral) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("equal");

  EXPECT_THROW(
      {
        try {
          SkiddoObject result = func.execute(ctx, {
                                                      SkiddoObject::quote(SkiddoObject::atom("a")),
                                                      SkiddoObject::literal(14),
                                                  });
        } catch (const TypeError& e) {
          EXPECT_STREQ("TypeError: 'a' object cannot be interpreted as a Literal", e.what());
          throw;
        }
      },
      TypeError);
}

TEST(ComparisonTest, ExpectSecondAsLiteral) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("equal");

  EXPECT_THROW(
      {
        try {
          SkiddoObject result =
              func.execute(ctx, {SkiddoObject::literal(14), SkiddoObject::quote(SkiddoObject::atom("b"))});
        } catch (const TypeError& e) {
          EXPECT_STREQ("TypeError: 'b' object cannot be interpreted as a Literal", e.what());
          throw;
        }
      },
      TypeError);
}
