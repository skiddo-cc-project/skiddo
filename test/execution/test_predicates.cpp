#include <gtest/gtest.h>

#include <memory>
#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>
#include <src/execution/standard_functions/standard_functions.hpp>

using execution::ProgramContext;
using types::SkiddoObject, types::Type, types::LiteralType;

class PredicateNormalTest
    // <function_name, object, expected_result>
    : public ::testing::TestWithParam<std::tuple<std::string, SkiddoObject, bool>> {};

TEST_P(PredicateNormalTest, NormalCase) {
  ProgramContext ctx = ProgramContext::standard_state();

  std::string function_name = std::get<0>(GetParam());
  SkiddoObject obj = std::get<1>(GetParam());
  bool expectedResult = std::get<2>(GetParam());

  SkiddoObject func = ctx.get_func(function_name);
  SkiddoObject res = func.execute(ctx, {obj});
  EXPECT_EQ(res.as_literal()->as_bool()->value, expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    PredicateNormalTests, PredicateNormalTest,
    testing::Values(std::make_tuple("isatom", SkiddoObject::quote(SkiddoObject::atom("setq")), true),
                    std::make_tuple("isatom", SkiddoObject::atom("setq"), false),
                    std::make_tuple("isnull", SkiddoObject::atom("setq"), false),
                    std::make_tuple("islist", SkiddoObject::quote(SkiddoObject::list({})), true),
                    std::make_tuple("islist", SkiddoObject::list({SkiddoObject::literal(5)}), false),
                    std::make_tuple("islist", SkiddoObject::literal(), false),
                    std::make_tuple("isint", SkiddoObject::literal(5), true),
                    std::make_tuple("isint", SkiddoObject::literal(5.7), false),
                    std::make_tuple("isint", SkiddoObject::literal(), false),
                    std::make_tuple("isreal", SkiddoObject::literal(-8.1), true),
                    std::make_tuple("isreal", SkiddoObject::literal(), false)));

TEST(PredicateTest, WrongArgumentCount) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("isatom");
  ASSERT_THROW(func.execute(ctx, {SkiddoObject::atom("setq"), SkiddoObject::atom("bob")}), TypeError);
}

TEST(PredicateTest, LiteralWrongArgumentCount) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("isint");
  ASSERT_THROW(func.execute(ctx, {SkiddoObject::atom("setq"), SkiddoObject::atom("bob")}), TypeError);
}
