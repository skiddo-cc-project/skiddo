#include <gtest/gtest.h>

#include <memory>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>

using execution::ProgramContext;
using types::SkiddoObject, types::Type, types::LiteralType;

TEST(ContextTest, SetAndGetVariable_ByStr) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject var = SkiddoObject::literal(3.4);
  ctx.register_func("some_var", var);

  SkiddoObject res = ctx.get_func("some_var");
  EXPECT_EQ(res.get_id(), var.get_id());
  ASSERT_EQ(res.get_type(), Type::LITERAL);

  types::Literal* res_literal = ((types::Literal*)res.get());
  ASSERT_EQ(res_literal->get_literal_type(), LiteralType::LITERAL_FLOAT);

  EXPECT_DOUBLE_EQ(((types::LiteralFloat*)res_literal)->value, 3.4);
}

TEST(ContextTest, ResetAndGetVariable_ByStr) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject var = SkiddoObject::atom("bb6u");
  ctx.register_func("aa", SkiddoObject::literal(false));
  ctx.register_func("aa", var);

  SkiddoObject res = ctx.get_func("aa");
  EXPECT_EQ(res.get_id(), var.get_id());
  ASSERT_EQ(res.get_type(), Type::ATOM);

  const std::string& name = ((types::Atom*)res.get())->name;
  EXPECT_STREQ(name.c_str(), "bb6u");
}
