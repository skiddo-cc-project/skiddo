#include <gtest/gtest.h>

#include <memory>
#include <skiddo/exceptions/runtime_exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>

using execution::ProgramContext;
using types::SkiddoObject, types::Type, types::LiteralType;

TEST(ExecutionTest, HeadFunctionNull) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("head");
  SkiddoObject res = func.execute(ctx, {SkiddoObject::quote(SkiddoObject::list({}))});
  EXPECT_EQ(res.as_literal()->get_literal_type(), LiteralType::LITERAL_NULL);
}

TEST(ExecutionTest, HeadFunctionWrongArg) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("head");
  SkiddoObject not_a_list = SkiddoObject::literal(0);
  EXPECT_THROW(func.execute(ctx, {not_a_list}), TypeError);
}

TEST(ExecutionTest, HeadFunctionWrongArgCount) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("head");
  SkiddoObject empty_list = SkiddoObject::list({});
  EXPECT_THROW(func.execute(ctx, {empty_list, SkiddoObject::atom("testingiscool")}), TypeError);
}

TEST(ExecutionTest, HeadFunctionSuccess) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("head");

  SkiddoObject first = SkiddoObject::literal(1);
  SkiddoObject list = SkiddoObject::list({first, SkiddoObject::literal(1), SkiddoObject::literal(1)});
  SkiddoObject res = func.execute(ctx, {SkiddoObject::quote(list)});
  EXPECT_EQ(res.get_id(), first.get_id());
}

TEST(ExecutionTest, TailFunctionSuccess) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("tail");

  SkiddoObject second = SkiddoObject::literal(2);
  SkiddoObject third = SkiddoObject::literal(3);
  SkiddoObject list = SkiddoObject::list({SkiddoObject::literal(1), second, third});
  SkiddoObject res = func.execute(ctx, {SkiddoObject::quote(list)});
  const std::vector<SkiddoObject>& items = res.as_list()->items;

  ASSERT_EQ(items.size(), 2);
  EXPECT_EQ(items[0].get_id(), second.get_id());
  EXPECT_EQ(items[1].get_id(), third.get_id());
}

TEST(ExecutionTest, TailFunctionWrongArg) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("tail");
  SkiddoObject not_a_list = SkiddoObject::literal(0);
  EXPECT_THROW(func.execute(ctx, {not_a_list}), TypeError);
}

TEST(ExecutionTest, TailFunctionWrongArgCount) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("tail");
  SkiddoObject empty_list = SkiddoObject::list({});
  EXPECT_THROW(func.execute(ctx, {empty_list, SkiddoObject::atom("testingiscool")}), TypeError);
}

TEST(ExecutionTest, ConsFunctionWrongArg) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("cons");

  SkiddoObject null_literal = SkiddoObject::literal();
  SkiddoObject not_a_list = SkiddoObject::quote(SkiddoObject::atom("not_a_list"));
  EXPECT_THROW(func.execute(ctx, {null_literal, not_a_list}), TypeError);
}

TEST(ExecutionTest, ConsFunctionWrongArgCount) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("cons");
  SkiddoObject obj = SkiddoObject::atom("testingiscool");
  EXPECT_THROW(func.execute(ctx, {obj, obj, obj}), TypeError);
}

TEST(ExecutionTest, ConsFunctionSuccess) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("cons");

  SkiddoObject first = SkiddoObject::literal(2);
  SkiddoObject second = SkiddoObject::literal(3);

  SkiddoObject literal = SkiddoObject::literal(0);
  SkiddoObject list = SkiddoObject::list({first, second});
  SkiddoObject res = func.execute(ctx, {literal, SkiddoObject::quote(list)});
  const std::vector<SkiddoObject>& items = res.as_list()->items;

  ASSERT_EQ(items.size(), 3);
  EXPECT_EQ(items[0].get_id(), literal.get_id());
  EXPECT_EQ(items[1].get_id(), first.get_id());
  EXPECT_EQ(items[2].get_id(), second.get_id());
}
