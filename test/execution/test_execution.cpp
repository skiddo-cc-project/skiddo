#include <gtest/gtest.h>

#include <memory>
#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>
#include <src/execution/standard_functions/standard_functions.hpp>

using execution::ProgramContext;
using types::SkiddoObject, types::Type, types::LiteralType;

TEST(ExecutionTest, SetAndGetVariable) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject var = SkiddoObject::literal(3.4);
  ctx.register_func("some_var", var);

  SkiddoObject res = SkiddoObject::atom("some_var").execute(ctx, {});
  EXPECT_EQ(res.get_id(), var.get_id());
  ASSERT_EQ(res.get_type(), Type::LITERAL);

  types::Literal* res_literal = ((types::Literal*)res.get());
  ASSERT_EQ(res_literal->get_literal_type(), LiteralType::LITERAL_FLOAT);

  EXPECT_DOUBLE_EQ(((types::LiteralFloat*)res_literal)->value, 3.4);
}

TEST(ExecutionTest, SetQAndGetVariable) {
  ProgramContext ctx = ProgramContext::standard_state();

  SkiddoObject var = SkiddoObject::literal(-3.4);
  SkiddoObject tree1 = SkiddoObject::list({SkiddoObject::atom("setq"), SkiddoObject::atom("_x3"), var});

  tree1.execute(ctx, {});

  SkiddoObject res = SkiddoObject::atom("_x3").execute(ctx, {});

  EXPECT_EQ(res.get_id(), var.get_id());
  EXPECT_DOUBLE_EQ(((types::LiteralFloat*)res.get())->value, -3.4);
}

TEST(ExecutionTest, IdentityLambda) {
  ProgramContext ctx = ProgramContext::standard_state();

  SkiddoObject lambda1 =
      ctx.get_func("lambda").execute(ctx, {SkiddoObject::list({SkiddoObject::atom("x")}), SkiddoObject::atom("x")});

  SkiddoObject value = SkiddoObject::literal(6334);
  SkiddoObject res = lambda1.execute(ctx, {value});
  EXPECT_EQ(res.get_id(), value.get_id());
}

TEST(ExecutionTest, CondFormTrue) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("cond");
  SkiddoObject first = SkiddoObject::literal(4);
  SkiddoObject res = func.execute(ctx, {SkiddoObject::literal(true), first});
  EXPECT_EQ(res.get_id(), first.get_id());
}

TEST(ExecutionTest, CondFormFalse) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("cond");
  SkiddoObject second = SkiddoObject::literal(7);
  SkiddoObject res = func.execute(ctx, {SkiddoObject::literal(false), SkiddoObject::literal(4), second});
  EXPECT_EQ(res.get_id(), second.get_id());
}

TEST(ExecutionTest, CondFormNull) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("cond");
  SkiddoObject res = func.execute(ctx, {SkiddoObject::literal(false), SkiddoObject::literal(4)});
  ASSERT_EQ(res.as_literal()->get_literal_type(), LiteralType::LITERAL_NULL);
}

TEST(ExecutionTest, BreakForm) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject val = SkiddoObject::literal(3.4);

  SkiddoObject command = SkiddoObject::list({SkiddoObject::atom("cond"), SkiddoObject::literal(false),
                                             SkiddoObject::literal(4), SkiddoObject::atom("break")});
  SkiddoObject whileBreak =
      SkiddoObject::list({SkiddoObject::atom("while"), SkiddoObject::literal(true), SkiddoObject::list({command})});
  SkiddoObject res = whileBreak.execute(ctx, {});
  ASSERT_EQ(res.as_literal()->get_literal_type(), LiteralType::LITERAL_NULL);
}

TEST(ExecutionTest, CustomFunction) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("func");

  func.execute(ctx,
               {SkiddoObject::atom("myXor"), SkiddoObject::list({SkiddoObject::atom("a"), SkiddoObject::atom("b")}),
                SkiddoObject::list({SkiddoObject::atom("xor"), SkiddoObject::atom("a"), SkiddoObject::atom("b")})});

  SkiddoObject tree1 =
      SkiddoObject::list({SkiddoObject::atom("myXor"), SkiddoObject::literal(true), SkiddoObject::literal(true)});

  SkiddoObject res = tree1.execute(ctx, {});
  EXPECT_EQ(res.as_literal()->as_bool()->value, false);
}

TEST(ExecutionTest, ReturnFunction) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("func");

  func.execute(ctx, {SkiddoObject::atom("identity"), SkiddoObject::list({SkiddoObject::atom("x")}),
                     SkiddoObject::list({SkiddoObject::atom("return"), SkiddoObject::atom("x")})});

  SkiddoObject tree1 = SkiddoObject::list({
      SkiddoObject::atom("identity"),
      SkiddoObject::literal(3.14),
  });

  SkiddoObject res = tree1.execute(ctx, {});
  EXPECT_DOUBLE_EQ(res.as_literal()->as_float()->value, 3.14);
}

TEST(ExecutionTest, ProgFunction) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("prog");

  SkiddoObject prog = func.execute(ctx, {SkiddoObject::list({}), SkiddoObject::list({SkiddoObject::list({
                                                                                         SkiddoObject::atom("and"),
                                                                                         SkiddoObject::literal(true),
                                                                                         SkiddoObject::literal(false),
                                                                                     }),
                                                                                     SkiddoObject::list({
                                                                                         SkiddoObject::atom("and"),
                                                                                         SkiddoObject::literal(true),
                                                                                         SkiddoObject::literal(true),
                                                                                     })})});

  SkiddoObject res = prog.execute(ctx, {});
  EXPECT_EQ(res.as_literal()->as_bool()->value, true);
}

TEST(ExecutionTest, ProgFunctionReturn) {
  ProgramContext ctx = ProgramContext::standard_state();
  SkiddoObject func = ctx.get_func("prog");

  SkiddoObject prog = func.execute(
      ctx, {SkiddoObject::list({SkiddoObject::atom("x"), SkiddoObject::atom("y")}),
            SkiddoObject::list({SkiddoObject::list(
                {SkiddoObject::atom("return"), SkiddoObject::list({SkiddoObject::atom("and"), SkiddoObject::atom("x"),
                                                                   SkiddoObject::atom("y")})})})});

  SkiddoObject res = prog.execute(ctx, {SkiddoObject::literal(true), SkiddoObject::literal(false)});
  EXPECT_EQ(res.as_literal()->as_bool()->value, false);
}
