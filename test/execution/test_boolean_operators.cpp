#include <gtest/gtest.h>

#include <skiddo/exceptions/exceptions.hpp>
#include <skiddo/execution/execution.hpp>
#include <skiddo/types/types.hpp>
#include <src/execution/standard_functions/standard_functions.hpp>
#include <string>
#include <tuple>

using execution::ProgramContext;
using types::SkiddoObject, types::Type, types::LiteralType;

class LogicalBinaryFunctionsTest
    // <function_name, left_operand, right_operand, expected_result>
    : public ::testing::TestWithParam<std::tuple<std::string, bool, bool, bool>> {};

TEST_P(LogicalBinaryFunctionsTest, NormalCase) {
  ProgramContext ctx = ProgramContext::standard_state();

  std::string function_name = std::get<0>(GetParam());
  bool left = std::get<1>(GetParam());
  bool right = std::get<2>(GetParam());
  bool expectedResult = std::get<3>(GetParam());

  SkiddoObject func = ctx.get_func(function_name);
  SkiddoObject res = func.execute(ctx, {SkiddoObject::literal(left), SkiddoObject::literal(right)});
  EXPECT_EQ(res.as_literal()->as_bool()->value, expectedResult);
}

INSTANTIATE_TEST_SUITE_P(
    LogicalBinaryFunctionsTests, LogicalBinaryFunctionsTest,
    testing::Values(std::make_tuple("and", false, false, false), std::make_tuple("and", false, true, false),
                    std::make_tuple("and", true, false, false), std::make_tuple("and", true, true, true),
                    std::make_tuple("or", false, false, false), std::make_tuple("or", false, true, true),
                    std::make_tuple("or", true, false, true), std::make_tuple("or", true, true, true),
                    std::make_tuple("xor", false, false, false), std::make_tuple("xor", false, true, true),
                    std::make_tuple("xor", true, false, true), std::make_tuple("xor", true, true, false)));

class LogicalUnaryFunctionsTests
    // <function_name, operand, expected_result>
    : public ::testing::TestWithParam<std::tuple<std::string, bool, bool>> {};

TEST_P(LogicalUnaryFunctionsTests, NormalCase) {
  ProgramContext ctx = ProgramContext::standard_state();

  std::string function_name = std::get<0>(GetParam());
  bool value = std::get<1>(GetParam());
  bool expectedResult = std::get<2>(GetParam());

  SkiddoObject func = ctx.get_func(function_name);
  SkiddoObject res = func.execute(ctx, {SkiddoObject::literal(value)});
  EXPECT_EQ(res.as_literal()->as_bool()->value, expectedResult);
}

INSTANTIATE_TEST_SUITE_P(LogicalOperatorsTest, LogicalUnaryFunctionsTests,
                         testing::Values(std::make_tuple("not", true, false), std::make_tuple("not", false, true)));
