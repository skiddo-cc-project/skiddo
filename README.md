# Skiddo programming language

This is a Compilers Construction project, taught in Innopolis University for third-year bachelor students. The compiler is used to compile the Skiddo language, which is untyped functional programming language inspired by Lisp.

You can read more about the language and its grammer [here](https://gitlab.com/skiddo-cc-project/skiddo/-/wikis/Language-Grammar).



## Building the project

To build the **release** version of the project first install some dependencies:

```bash
apt-get install libreadline-dev
```

Build the project:

```bash
mkdir build && cd build
cmake .. && make -j4
```

Now you can run the project:

```bash
./skiddo fileName.skd
```

## Running linter, tests and measure coverage

Run linter (you need to install [cpplint](https://github.com/cpplint/cpplint)):

```bash
./linter.sh
```

Run formatter (you need to install [clang-format](https://clang.llvm.org/docs/ClangFormat.html)):

```bash
./formatter.sh
```

Build project with tests and coverage support:

```bash
mkdir build && cd build
cmake -DBUILD_TESTS=ON -DCMAKE_BUILD_TYPE=Coverage .. && make -j$(nproc)
```

Run tests and coverage measurement (you need to install [gcovr](https://gcovr.com/en/stable/installation.html)):

```bash
make test && make coverage
```

## Released Version 

You can download the prebuild binaries from [here](https://gitlab.com/skiddo-cc-project/skiddo/-/jobs/artifacts/master/file/skiddo?job=build_prod) 


## 💻 Contributers

<p>

  :mortar_board: <i>All participants in this project are undergraduate students in the <a href="https://apply.innopolis.university/en/bachelor/">Department of Computer Science</a> <b>@</b> <a href="https://innopolis.university/">Innopolis University</a></i>. <br> <br>

  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>v.markov@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>

  :boy: <b>Gleb Osotov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>g.osotov@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitHub: <a href="https://github.com/glebosotov">@glebosotov</a> <br>

  :boy: <b>Igor Belov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>i.belov@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitHub: <a href="https://github.com/igooor-bb">@igooor-bb</a> <br>

  :girl: <b>Tasneem Toolba</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>t.samir@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitHub: <a href="https://github.com/tasneem22">@tasneem22</a> <br>
</p>

